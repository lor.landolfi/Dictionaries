#include "AllIncludes.h"
#include "Benchmark_common.hpp"

using namespace std;

template<typename T>
bool mygreater (T i,T j) { return (i>=j); }

template<typename T>
uint64_t binary_search(const std::vector<T>& vec, uint64_t start, uint64_t end, const uint64_t key)
{
    // Termination condition: start index greater than end index
   while(end != start){
    unsigned middle = ((start + end )/2)+1;


    if (vec[middle] > key){
        end = middle-1;
    }
    else {
        start = middle;
    }
}
return start;
}


template<typename Iterator, typename T>
Iterator Binary_search(Iterator& begin, Iterator& end, const T& key)
{
    // Keep halving the search space until we reach the end of the vector
    Iterator NotFound = end;
 
    while(begin < end)
    {
        // Find the median value between the iterators
        Iterator Middle = begin + (std::distance(begin, end) / 2);
 
        // Re-adjust the iterators based on the median value
        if(*Middle == key)
        {
            return Middle;
        }
        else if(*Middle > key)
        {
            end = Middle;
        }
        else
        {
            begin = Middle + 1;
        }
    }
 
    return NotFound;
}