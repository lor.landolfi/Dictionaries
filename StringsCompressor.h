/*
 * StringsCompressor.h
 *
 *  Created on: 11/dic/2013
 *      Author: lorenzo
 */

#ifndef STRINGSCOMPRESSOR_H_
#define STRINGSCOMPRESSOR_H_
#include "AllIncludes.h"

namespace std {

class StringsCompressor {
public:

	inline StringsCompressor();
	inline virtual ~StringsCompressor();
	virtual bool Append(string s)=0;
	virtual void Flush(bool final)=0;
	virtual std::vector<uint64_t>* GetPositions()=0;
	virtual std::vector<uint64_t>* GetUncompressed()=0;
	virtual uint64_t GetTotalBytesI()=0;
	virtual uint64_t GetTotalBytesS()=0;
	virtual uint64_t GetTotalBytesU()=0;
	virtual uint64_t GetStringNumber()=0;
	virtual uint64_t GetCopied()=0;
	virtual void clear()=0;
	virtual void Clear()=0;
	virtual uint GetParameter()=0;
	virtual uint GetBlock()=0;
	virtual string Get_Last_String()=0;
	virtual void Append_LAST()=0;

};

inline StringsCompressor::StringsCompressor(){}
inline StringsCompressor::~StringsCompressor(){}

} /* namespace std */
#endif /* STRINGSCOMPRESSOR_H_ */
