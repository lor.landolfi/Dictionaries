//struct __float128;
#include <string>
#include <array>

#include "encoders.hpp"
#include <stdio.h>
#include <iostream>
//#include <common.hpp>
//#include <cost_model.hpp>

namespace lzopt {

namespace hybrid {

const unsigned int masks[] = {0x3F, 0x3FFF, 0x3FFFFF, 0x3FFFFFFF};
const unsigned int l_masks[] = {0x7F, 0x7FFF};

}

}

namespace Fastest {
	//const unsigned int masks[] = {0x7F, 0x7FFF};

}

namespace soda09 {

std::array<unsigned int, 7> soda09_dst::cost_classes =
{
	0U,
	16384U,
	278528U,
	2375680U,
	19152896U,
	153370624U,
	1227112448U
};


std::array<unsigned int, 6> soda09_dst::decode_mask =
{
	(1U << binary_width[0]) - 1,
	(1U << binary_width[1]) - 1,
	(1U << binary_width[2]) - 1,
	(1U << binary_width[3]) - 1,
	(1U << binary_width[4]) - 1,
	(1U << binary_width[5]) - 1
};

std::array<unsigned int, 6> soda09_dst::binary_width =
{
	14U, 18U, 21U, 24U, 27U, 30U
};

std::array<unsigned int, 16> soda09_len::cost_classes =
{
	0U,
	8U,
	16U,
	24U,
	32U,
	48U,
	64U,
	80U,
	112U,
	176U,
	304U,
	560U,
	1072U,
	2096U,
	4144U,
	1052720U
};

std::array<unsigned int, 15> soda09_len::binary_width =
{
	3U,
	3U,
	3U,
	3U,
	4U,
	4U,
	4U,
	5U,
	6U,
	7U,
	8U,
	9U,
	10U,
	11U,
	20U
};

std::array<unsigned int, 15> soda09_len::decode_mask =
{
	(1U << binary_width[0]) - 1,
	(1U << binary_width[1]) - 1,
	(1U << binary_width[2]) - 1,
	(1U << binary_width[3]) - 1,
	(1U << binary_width[4]) - 1,
	(1U << binary_width[5]) - 1,
	(1U << binary_width[6]) - 1,
	(1U << binary_width[7]) - 1,
	(1U << binary_width[8]) - 1,
	(1U << binary_width[9]) - 1,
	(1U << binary_width[10]) - 1,
	(1U << binary_width[11]) - 1,
	(1U << binary_width[12]) - 1,
	(1U << binary_width[13]) - 1,
	(1U << binary_width[14]) - 1,
};

}

namespace nibble {

std::array<unsigned int, 11> class_desc::cost_classes =
{
	0U,
	8U,
	72U,
	584U,
	4680U,
	37448U,
	299592U,
	2396744U,
	19173960U,
	153391688U,
	1227133512U,
};

std::array<unsigned int, 10> class_desc::binary_width =
{
	3U, 6U, 9U, 12U, 15U, 18U, 21U, 24U, 27U, 30U
};

std::array<unsigned int, 10> class_desc::decode_mask =
{
	(1U << binary_width[0]) - 1,
	(1U << binary_width[1]) - 1,
	(1U << binary_width[2]) - 1,
	(1U << binary_width[3]) - 1,
	(1U << binary_width[4]) - 1,
	(1U << binary_width[5]) - 1,
	(1U << binary_width[6]) - 1,
	(1U << binary_width[7]) - 1,
	(1U << binary_width[8]) - 1,
	(1U << binary_width[9]) - 1,
};

}



/*
int main(int argc, char const *argv[])
{
	unsigned char a,aca;
	byte *b;
	b = (byte*) malloc (4*sizeof(byte));
	//a = (char*) malloc (4);
	std::uint32_t val;
	std::uint32_t cicci = 0;
	for (uint64_t i=0; i<(259); i++){
		cicci = i;
		lzopt::hybrid::dst_encode(cicci,b);
	 	lzopt::hybrid::dst_decode(b,&val);
	 	if (val != cicci){
	 		cout << "aia " << cicci << " " << val << endl;
	 	}
	 	memset(b,0,4);

	}
	std::uint_fast32_t ciccino = 30000;
	cout << "WOW" << endl;

	//cout << val << endl;

	//unaligned_io::writer *writer = new unaligned_io::writer(b); 



	//std::cout << "ciao this is FUNDAMENTAL! " << val-1 << std::endl;
	return 0;
}*/
