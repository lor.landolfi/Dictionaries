
#include "encoders.hpp"
//#include "Benchmark_common.hpp"
#include <utility>  
#include <iostream>
#include <boost/shared_ptr.hpp>
#include <boost/make_shared.hpp>
#include <vector>
#include <sys/types.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>
#include "Inserters.hpp"

using namespace std;

class benchmark
{
public:
    virtual ~benchmark() {}
    //questo potrebbe generare numeri casuali e mapparli in un file
    virtual void Prepare(uint number_of_numbers, const string &output_path)=0;
    //questo potrebbe mappare il file in File_path in un array di stringhe e generare lunghezze suffissi,lpcs, rears.
    virtual void Prepare(const string& File_path) = 0;
    virtual size_t Benchmark_encode(uint16_t* sizes, uint filesize,byte* output,uint* offsets)=0;
    virtual void Benchmark_Decode(byte* buf, const std::vector<uint> &v, uint* &offsets)=0;
    virtual void Benchmark_Decode(byte* buf, const uint v)=0;
};

template <typename E>
class Benchmarker : public benchmark {

public:
	E Encoder;

	Benchmarker() : Encoder(){};

virtual void Prepare(uint number_of_numbers, const string &output_path){


}

virtual void Prepare(const string& File_path){

}

virtual size_t Benchmark_encode(uint16_t* sizes, uint filesize,byte* output,uint* offsets){
uint Int_numbers = filesize/sizeof(uint16_t);

short tag=0;
uint64_t space = 0;
for (uint i=0; i<Int_numbers; i++){
//	cout << "encoding " << sizes[i] << endl;
	tag = Encoder.Encode(sizes[i],output+space);
	space = space + tag;
	offsets[i] = space-tag;
}
cout << "Occupied space with " << Encoder.GetName() << " " << space << endl;
return space;
}

virtual void Benchmark_Decode(byte* beginning, const uint v){
	short tag = 0;
	cout << "v: " << v << endl;
	std::vector<byte*> byte_pointers;
	for (int k=0; k<10;k++){
		byte_pointers.push_back(beginning);
	}
	byte* out;
	uint16_t value = 0;
		TIMEIT("consecutive decoding: "+Encoder.GetName(), v*10){
			for (int j=0; j<10; j++){
			for (uint64_t i= 0; i<v; i++){
				byte_pointers[j]  = Encoder.Decode(byte_pointers[j],&value);
				//byte_pointers[j]  = lzopt::hybrid::len_decode(byte_pointers[j],&value);
				//cout << "decoded " << value << endl;
				//value = 0;
				
			}
		}
		}
}


//takes a vector of offsets for random access decoding
virtual void Benchmark_Decode(byte* beginning, const vector<uint> &v, uint* &offsets){
	size_t size_v = v.size();
	cout << size_v << endl;
	short tag = 0;
	uint16_t value = 0;
	uint16_t value2 = 0;
	byte* out = beginning;
	byte* g;
	uint64_t sum =0;
		TIMEIT("random decoding: "+Encoder.GetName(), size_v*10){
			for (int j=0; j<10; j++){
			for (uint64_t i= 0; i<size_v/2; i=i+2){

			//cout << offsets[v[i]] << endl;
				out = Encoder.Decode(beginning+offsets[v[i]],&value);
				sum = sum + value;
				g = g + (unsigned long)out++;
				out = Encoder.Decode(beginning+offsets[v[i+1]],&value2);
				sum = sum+value;
				g = g +(unsigned long)out++;
				//value = 0;
				//cout << "decoded " << value << endl;
			}
		}
		}
		cout <<sum<<endl;
		cout <<(unsigned long)g << endl;
}
};

class Gamma_benchmark : public benchmark{

	virtual void Prepare(uint number_of_numbers, const string &output_path){}
   
    virtual void Prepare(const string& File_path){}

	virtual size_t Benchmark_encode(uint16_t* sizes, uint filesize,byte* output,uint* offsets){
		unaligned_io::writer writer(output);
		uint Int_numbers = filesize/sizeof(uint16_t);
		uint32_t ret = 0;
		uint64_t space = 0;
		for (uint i =0; i<Int_numbers; i++){
			ret = gamma_like::encode<soda09::soda09_len>(sizes[i],writer);
			space = space +ret;
		}

		cout << "Occupied space with Gamma: "  << space/8 << endl;
		return space;
	}

	virtual void Benchmark_Decode(byte* beginning, const uint v){
		cout << "v: " << v << endl;
		std::vector<byte*> byte_pointers;
		for (int k=0; k<10;k++){
			byte_pointers.push_back(beginning);
		}
		uint32_t ret = 0;
		uint64_t sum =0;
		TIMEIT("consecutive decoding with gamma: ", v*10){
			for (int j=0; j<10; j++){
				unaligned_io::reader reader(byte_pointers[j]);
			for (uint64_t i= 0; i<v; i++){
				ret  = gamma_like::decode<soda09::soda09_len>(reader);	
				sum = sum + ret;
				//cout << ret << endl;			
			}
		}

		}
		cout <<sum<<endl;

	}

	   virtual void Benchmark_Decode(byte* buf, const std::vector<uint> &v, uint* &offsets) {}



};

typedef std::map<std::string, boost::shared_ptr<benchmark> > benchmarks_type;

void print_benchmarks(benchmarks_type const& benchmarks)
{
    std::cerr << "Available benchmarks: " << std::endl;
    for (benchmarks_type::const_iterator iter = benchmarks.begin();
         iter != benchmarks.end();
         ++iter) {
        std::cerr << iter->first << std::endl;
    }
}

int main (int argc, char**argv){
	if (argc == 1 ){
		cout << "Integer decoding benchmark \n To use specify a binary file of unsigned integers " << endl;
		return 1;
	}

	
	using boost::shared_ptr;
    using boost::make_shared;

    benchmarks_type benchmarks;
    benchmarks["VBFast"] = make_shared<Benchmarker <lzopt::hybrid::Encode_Wrapper> >();
    benchmarks["VarByte"] = make_shared<Benchmarker <MSB::Encode_Wrapper> >();
    benchmarks["VBFast15"] = make_shared<Benchmarker <Fastest::Encode_Wrapper> >();
    benchmarks["GammaPadding"] = make_shared<Benchmarker <Gamma_encoder> >();
    benchmarks["Gamma"] = make_shared<Gamma_benchmark>();


    if (argc != 3){
    	print_benchmarks(benchmarks);
    	return 1;
    }
    else {
    struct stat sb;
	int fd;

	fd = open(argv[2], O_RDONLY);
	fstat(fd, &sb);
	size_t filesize = sb.st_size;
	//map to know the size of each line of the file
	uint16_t* sizes = (uint16_t*)mmap(NULL, sb.st_size, PROT_READ, MAP_SHARED, fd, 0);
	uint* offsets = (uint*) malloc((filesize/2)*sizeof(uint));
	
	srand(40);
	std::vector<uint> random_numbers;
	random_numbers.reserve(filesize/2);
	for (uint i=0; i<filesize/2; i++){
		random_numbers.push_back((rand() * RAND_MAX + rand()) % (filesize/2));
	}
	byte* buffer = (byte*)malloc(filesize);
	
    	const char *b = argv[1];
    	if (!benchmarks.count(b)) {
            std::cerr << "No benchmark " << b << std::endl;
            print_benchmarks(benchmarks);
            return 1;
        }
        else {
        	shared_ptr<benchmark> myptr = benchmarks[b];
        	cout << "Benchmark allocated " << endl;
        	size_t space = myptr->Benchmark_encode(sizes,filesize,buffer,offsets);
        	myptr->Benchmark_Decode(buffer,filesize/sizeof(uint16_t));
        	myptr->Benchmark_Decode(buffer,random_numbers,offsets);
        }
    }

}