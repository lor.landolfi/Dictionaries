OBJECTS := StringFunctions.cpp ./succinct/bp_vector.cpp ./succinct/rs_bit_vector.cpp encoders.cpp unaligned_io.cpp


INCLUDES := -I /usr/local/boost_1_54_0

indexer: Indexer.cpp $(OBJECTS)
	clang++ -std=gnu++11 -lboost_iostreams $(LIBS) $(INCLUDES) -g $(OBJECTS) -O2 Indexer.cpp -o ../bin/Indexer.o

triemaker: TrieMaker.cpp $(OBJECTS)
	clang++ -std=gnu++11 $(LIBS) $(INCLUDES) -g $(OBJECTS) -O2 TrieMaker.cpp -o ../bin/Triemaker.o

retriever: Retriever.cpp $(OBJECTS)
	clang++ -std=gnu++11 $(LIBS)-lboost_iostreams -lboost_system $(INCLUDES) -g $(OBJECTS) -O2 -lboost_program_options  Retriever.cpp -o ../bin/Retriever.o

searcher: Retriever.cpp $(OBJECTS)
	clang++ -std=gnu++11 $(LIBS)-lboost_iostreams -lboost_system $(INCLUDES) -g $(OBJECTS) -O3  -lboost_program_options Prefix_Searcher.cpp -o ../bin/Prefix_Searcher.o

integer: Integer_encoding_benchmark.cpp
	clang++ -std=gnu++11 -I /usr/local/boost_1_54_0 -g StringFunctions.cpp ./succinct/bp_vector.cpp ./succinct/rs_bit_vector.cpp encoders.cpp unaligned_io.cpp -O3 		Integer_encoding_benchmark.cpp -o ../bin/Integer_encoding_benchmark.o

distribution: Contadimensione.cpp
	clang++ -std=gnu++11 -O2 -I /usr/local/boost_1_54_0 Contadimensione.cpp -o ../bin/Contadimensione.o

succinct: Benchmark_succinct.cpp
	clang++ -std=gnu++11 -I /usr/local/boost_1_54_0 -lboost_iostreams -O3 -g ./succinct/bp_vector.cpp ./succinct/rs_bit_vector.cpp  Benchmark_succinct.cpp -o ../bin/Benchmark_succinct.o

binary_search: Binary_search.cpp
	clang++ -std=gnu++11 -I /usr/local/boost_1_54_0 -lboost_iostreams Binary_search.cpp -O3  -o ../bin/Binary_search

Measurer: Benchmark_Retrieval.cpp $(OBJECTS)
	clang++ -std=gnu++11 -lboost_iostreams $(LIBS) $(INCLUDES) -g $(OBJECTS) -O3 Benchmark_Retrieval.cpp -o ../bin/Benchmark_Retrieval.o

Prefix:	Find_prefixes.cpp
	clang++ -std=gnu++11 -lboost_iostreams $(LIBS) $(INCLUDES) -g $(OBJECTS) -O3 Find_prefixes.cpp -o ../bin/Find_prefixes.o

tst: Test_TST.cpp
	clang++ -std=gnu++11 -O3 Test_TST.cpp $(OBJECTS) -o ../bin/Test_TST.o 

