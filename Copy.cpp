#include "CCOSBtree.hpp"

int main(int argc, char** argv){

		if (argc != 2){
			cout << "Enter input file name" << endl;
			return 1;
		}
		int fd;
		size_t filesize;
		//information of the file
		struct stat sb;

		fd = open(argv[1], O_RDONLY);
		fstat(fd, &sb);
		filesize = sb.st_size;

		char * file = (char*)mmap(NULL,sb.st_size, PROT_READ, MAP_SHARED, fd, 0);

		char* buffer = (char*)malloc(filesize*sizeof(char));

		char* pointer_to_file;
		char* pointer_to_buffer;
		  TIMEIT("Time to copy: ",filesize/sizeof(char)){
		  	pointer_to_file =(char*) &file[0];
		  	pointer_to_buffer =(char*) &buffer[0];
		  //	uint64_t loop_l= (filesize+7)/8;
		  	uint64_t loop_l = filesize;

		  	for (uint64_t i=0; i<loop_l; i++){
		  		*pointer_to_buffer = *pointer_to_file;
		  		pointer_to_file++;
		  		pointer_to_buffer++;
		  	}

		  }



}