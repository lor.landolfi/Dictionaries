//struct __float128;
#include "StringsCompressor.h"

using namespace std;

uint Lcp2 (const char* a, const char* b, uint a_dim, uint b_dim);

uint LcpS (const string &a,  const string &b, int off_a=0, int off_b=0);

std::string EmitString(uint64_t a, const std::vector<uint> &lcpArray, const std::vector<string> &stringset);

string EmitString(uint64_t a,const uint* t, succinct::elias_fano &E, char* f);

std::vector<uint> LCP_Builder (char** strings, const std::vector<uint> &dimensions);

uint64_t LCP_Builder (succinct::elias_fano &E,char*f,const string location);

void TrieVisitSimulationSuccinct(char ** stringset,  const std::vector<uint> &dimensions,std::ofstream *output, const std::vector<uint> &lcpArray);

void TrieVisitSimulationSuccinct(succinct::elias_fano &E,char* f,std::ofstream *output, uint* lcpArray, uint64_t lcpArray_size );

void TrieVisitUtil(const std::vector<uint> &lcpArray,succinct::cartesian_tree * t,char** strings, const std::vector<uint> &dimensions,std::ofstream *output, uint64_t i,uint64_t j);

void TrieVisitUtil(const uint* lcpArray,succinct::cartesian_tree * t,succinct::elias_fano &E,std::ofstream *output, uint64_t i,uint64_t j);

std::ifstream::pos_type filesize(const char* filename);
