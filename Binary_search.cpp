
#include "Binary_search.h"


int main (int argc, char**argv){

    if (argc !=3 ){
        cerr << "help: " <<endl;
        cerr << "Input arguments: binary array of integers, strings file" << endl;
    }

    succinct::util::mmap_lines strings_lines(argv[2]);
    std::vector<std::string> strings(strings_lines.begin(), strings_lines.end());
    size_t m_size = strings.size();

//map a vector of uncompressed vector
        int fd;
        size_t filesize,filesize_E,filesize_V,filesize_orig;
        //information of the file
        struct stat sb;

        fd = open(argv[1],O_RDONLY);
        fstat(fd, &sb);
        filesize = sb.st_size;
        std::vector<uint32_t> v;
        uint32_t *U_vector = (uint32_t*)mmap(NULL, sb.st_size, PROT_READ, MAP_SHARED, fd, 0);
        v.reserve(filesize/4);
        //int key_index;

        uint64_t max_index = U_vector[(filesize/4)-1];
        cout << "max_index: " << max_index << endl;

        for (uint i=0; i< filesize/4; i++){
            v.push_back(U_vector[i]);
        }
        uint32_t* iter_beg = &U_vector[0];
        uint32_t* iter_end = &U_vector[filesize/4];
        uint64_t k_sum = 0;

        std::vector<uint64_t> Random;
        Random.reserve(m_size);
        srand(40);
        for (uint64_t i =0; i<m_size; i++){
        Random.push_back((rand() * RAND_MAX + rand())%m_size);
    }

        TIMEIT("consecutive searches TIME_ITERATION",m_size*10){
            for (int j=0; j<10; j++){
            for (uint64_t i =0; i<m_size; i++){
               // cout << v[i] << endl;
              uint64_t key_index = binary_search<uint32_t>(v,0,(filesize/4),i);
                k_sum = k_sum + key_index;
                //std::cout << key_index << std::endl;
             
            }
        }
    }
    cout << k_sum << endl;
    k_sum =0;
    std::vector<uint32_t>::iterator beg = v.begin();
    std::vector<uint32_t>::iterator end = v.end();

   
    TIMEIT("random searches TIME_ITERATION",m_size*10){
        for (int j=0; j<10; j++){
            for (uint64_t i=0; i<m_size; i++){
               uint64_t key_index = binary_search<uint32_t>(v,0,(filesize/4)-1,Random[i]);
              // cout << Random[i] << " " << v[key_index] << endl;
                k_sum = k_sum + key_index;
            }
        }
    }
    cout << k_sum << endl;
    std::vector<uint32_t>::iterator low;

     TIMEIT("consecutive searches std::upper_bound TIME_ITERATION",m_size*10){
            for (int j=0; j<10; j++){
            for (uint64_t i =0; i<m_size; i++){
               // cout << v[i] << endl;
                low = std::upper_bound(beg,end,i);
               uint64_t key_index = low - 1 -beg;
               // cout << "lower_bound at position " << low - v.begin() << endl;
              // uint key_index = binary_search<uint32_t>(v,0,(filesize/4),i);
                k_sum = k_sum + key_index;
                //std::cout << key_index << std::endl;
             
            }
        }
    }
    cout << k_sum << endl;
    k_sum =0;

     TIMEIT("random searches std::upper_bound TIME_ITERATION",m_size*10){
            for (int j=0; j<10; j++){
            for (uint64_t i =0; i<m_size; i++){
               // cout << v[i] << endl;
                low = std::upper_bound(beg,end,Random[i]);
                uint64_t key_index = low -1 -beg;
               // cout << "lower_bound at position " << low - v.begin() << endl;
              // uint key_index = binary_search<uint32_t>(v,0,(filesize/4),i);
                k_sum = k_sum + key_index;
                //std::cout << key_index << std::endl;
             
            }
        }
    }
    cout << k_sum << endl;
    k_sum =0;

}