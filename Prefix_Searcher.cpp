#include "CCOSBtree.hpp"

#include <sys/time.h>
#include <sys/resource.h>
#include <boost/program_options.hpp>
#include <functional>

namespace po = boost::program_options;



inline double get_time_usecs() {
        timeval tv;
        gettimeofday(&tv, NULL);
        return double(tv.tv_sec) * 1000000 + double(tv.tv_usec);
    }

    inline double get_user_time_usecs() {
        rusage ru;
        getrusage(RUSAGE_SELF, &ru);
        return double(ru.ru_utime.tv_sec) * 1000000 + double(ru.ru_utime.tv_usec);


    }

    		static const char alphanum[] =
"0123456789"
"ABCDEFGHIJKLMNOPQRSTUVWXYZ"
"abcdefghijklmnopqrstuvwxyz";

    char genRandom()  // Random string generator function.
{

    return alphanum[rand() % (48+9)];
}

typedef std::vector<uint64_t> (CCOSBtree<VBFastCoder <VBByte_Encoder > >::*pprefix)(string&, succinct::elias_fano &, succinct::rs_bit_vector &, succinct::cartesian_tree&);
typedef std::vector<uint64_t> (CCOSBtree<VariantCoder <VBByte_Encoder> >::*pprefix2)(string&, succinct::elias_fano &, succinct::rs_bit_vector &, succinct::cartesian_tree&);






	
    
    //Ex: ./Retriever.o 2gram.txt_FC_100_VBFast 
    int main (int argc, char**argv){


    	bool slow = false;
    	bool randomized = false;
    	int strings_to_retrieve = 0;


    	// Declare the supported options.
po::options_description desc("Allowed options");

desc.add_options()
    ("help", "produce help message")
    ("r", po::value<int>() ,"retrieves in a randomized way the indicated number of strings")
    ("s", "uses the slow retrieval function")
    ("in",po::value<string>(),"input-file");

    po::positional_options_description p;
    p.add("in",-1);

    po::variables_map vm;
    po::store(po::command_line_parser(argc, argv).options(desc).positional(p).run(), vm);
	po::notify(vm);    

if (vm.count("help")) {
    cout << desc << "\n";
    return 1;
}

if (vm.count("in")) {
	//cout << "input file is: " << vm["in"].as<string>() << endl;
}

if(vm.count("r")){
	randomized = true;
	strings_to_retrieve = vm["r"].as<int>();
}

if(vm.count("s")){
	slow = true;
}
     

		std::string inputfilename (vm["in"].as<string>());

		CCOSBtree<VBFastCoder < VBFast_Encoder> > tree(vm["in"].as<string>(),true);
		CCOSBtree<VariantCoder < VBFast_Encoder> > treeV(vm["in"].as<string>(),true);

  

	

    	succinct::elias_fano E;
    	succinct::rs_bit_vector V;
        succinct::cartesian_tree T;


    	tree.MaxString_size = 0;
    	std::string line;

    	std::string inputArg(inputfilename);
    	size_t lastBar = inputArg.find_last_of ('/');
    	size_t F_index = inputArg.find("_tried_",lastBar);
    	if (lastBar == string::npos){
    		lastBar = 0;
    	}
  

    	if (F_index != -1){
    	inputArg = inputArg.substr(0,F_index+6);
    }
    else {
    	F_index = inputArg.find('_',lastBar);
    	inputArg = inputArg.substr(0,F_index);
    }
    	
    

    	uint64_t j=0;


		
		
		string EliasFanoLoc = inputfilename+"_Elias";
		string VvectorLoc = inputfilename+"_V_vector";
        string LCP_loc = inputfilename+"_lcp_cartesian";
		string FileLoc = inputfilename;

       // cout << "cartesian tree location: " << LCP_loc << endl;

		int fd;
		size_t filesize,filesize_E,filesize_V;
		//information of the file
		struct stat sb;

		//get the file descriptors of each file: compressed dictionary, elias-fano vector, V-vector
		fd = open(FileLoc.c_str(),O_RDONLY);
		fstat(fd, &sb);
		filesize = sb.st_size;

		fd = open(EliasFanoLoc.c_str(),O_RDONLY);
		fstat (fd,&sb);
		filesize_E = sb.st_size;

		 fd = open(VvectorLoc.c_str(),O_RDONLY);
		fstat (fd,&sb);
		filesize_V = sb.st_size;




		//map the elias-fano file in elias fano object E

		boost::iostreams::mapped_file_source m(EliasFanoLoc.c_str());
		size_t quindi = succinct::mapper::map<succinct::elias_fano>(E,m);
		//get the number of strings
	
		//map the V-vector in re_bit_vector object V
		boost::iostreams::mapped_file_source m2(VvectorLoc.c_str());
		size_t dunque = succinct::mapper::map<succinct::rs_bit_vector>(V,m2);
		j = V.size()-1;
		

        boost::iostreams::mapped_file_source m3(LCP_loc.c_str());
        size_t anche = succinct::mapper::map<succinct::cartesian_tree>(T,m3);

        



		int stringLength = sizeof(alphanum) - 1;

	


    	double begin,end,beginU,endU,elapsed_sec,elapsed_secU;
    	std::vector<string> v;
    	//v.reserve(strings_to_retrieve);
    	string b;
    	srand(40);
    
    		if (randomized == true){
    			
    			for (int i = 0; i< strings_to_retrieve; i++){
    				for(unsigned int i = 0; i < rand()%10; ++i)
					{
    				b += genRandom();
					}
					v.push_back(b);
                    b.clear();
    			}
    		}

		

  
    	tree.MaxString_size = 1000000;
    	tree.MaxString = (char*)malloc((tree.MaxString_size+8)*sizeof(char));
    	treeV.MaxString_size = 1000000;
    	treeV.MaxString = (char*)malloc((treeV.MaxString_size+8)*sizeof(char));
    	std::vector<uint64_t> Ranges;

        pprefix pp = NULL;
        pprefix2 pp2 = NULL;
    


        if (slow){
            cout << "slow" << endl;
      
            pp = &CCOSBtree<VBFastCoder <VBFast_Encoder > >::WrapperFunction;
            pp2 = &CCOSBtree<VariantCoder <VBFast_Encoder> >::WrapperFunction;
        }
        else {
      
            pp = &CCOSBtree<VBFastCoder <VBFast_Encoder > >::Prefix_Search_Fast;
            pp2 = &CCOSBtree<VariantCoder<VBFast_Encoder> >::Prefix_Search_Fast;
        }

		double time1= get_time_usecs();
        double time1_u = get_user_time_usecs();
    if (inputfilename.find("VBFast") != string::npos){
		for (int i = 0; i< strings_to_retrieve; i++){   

    	Ranges = (tree.*pp)(v.at(i),E,V,T);
    	
      
    }
}
else {
	
    for (int i = 0; i< strings_to_retrieve; i++){    
        Ranges = (treeV.*pp2)(v.at(i),E,V,T);

    }
}
    

    double time2 = get_time_usecs();
    double time2_u = get_user_time_usecs();
    double elapsed_time = time2 - time1;
    double elapsed_time_u = time2_u - time1_u;

    cout << "{" << endl;
   
        cout << "\"Average prefix search time\" : \""+to_string(elapsed_time/v.size())+"\" , \"Average user prefix search time\" :\""+to_string(elapsed_time_u/v.size())+"\"" << endl;


    cout << "}" << endl;

    
 



    }
