/*
 * CCOSBtree.h
 *
 *  Created on: 27/nov/2013
 *      Author: lorenzo
 */

#ifndef CCOSBTREE_H_
#define CCOSBTREE_H_

struct __float128;
#include "Inserters.hpp"
#include "TST.hpp"

int compare(unsigned char *a, unsigned char *b) {

    while(true) {

        if ( *a != *b ) { return (*a < *b ) ? -1 : 1; }

        a++; b++;

    }

   return 0;

}

 


namespace std {


//this class must produce output files: compressed strings, V and E vectors
template <class CODER>
class CCOSBtree {
public:

	StringsCompressor *inserter;
	char* addr;				//the starting address of file
	size_t filesize;		//file size
	uint64_t strings;		//number of strings
	uint64_t LastUncompressed;
	CODER coder;
	char* MaxString;
	uint64_t MaxString_size;
	string InputLoc;
	uint NumCopied;
	std::vector<string> The_copieds;
	size_t Byte_size;


	//per retrieval allocare buffer grande come la più grande stringa e restituire quello
	//Costruttore per salvare le strutture dati
	CCOSBtree(uint c,uint d,bool bucket,std::string &inputfilename,string &outputfilename,bool opt) : coder() {
		std::ifstream in(inputfilename.c_str());
				
		std::ofstream out(outputfilename.c_str());
			

		if (!bucket){
			inserter = new LPCStructure<CODER>(c,d,&out);
		
		}
		else{
			inserter =  new BucketCoder<CODER>(c,d,&out);
		}

		std::string line;

		uint j=0;
		string FIRST ="";
		FIRST = FIRST+(char)0x0000;
		inserter->Append(FIRST);
		bool UNC = false;
		//The_copieds.push_back(FIRST);
		j++;
		while (std::getline(in, line))
		{
			std::istringstream iss(line);
		//	cout << line << endl;
			if (inserter->Append(line)){
				The_copieds.push_back(line);
			}
			j++;
		}

		inserter->Append_LAST();
		j++;
	
		//cout << "All appended" << endl;	
		inserter->Flush(true);
		out.close();
		
		uint64_t k=0;

		//starting address to map the file where compressed strings are stored
		//file descriptor
		int fd,fd_U,fd_P;
		//information of the file
		struct stat sb,sb_U,sb_P;

		fd = open(outputfilename.c_str(), O_RDONLY);
		fstat(fd, &sb);
		filesize = sb.st_size;

		char * file = (char*)mmap(NULL,sb.st_size, PROT_READ, MAP_SHARED, fd, 0);

		fd_U = open("Uncompressed_vector", O_RDONLY);
		fstat(fd_U,&sb_U);
		size_t filesize_U=sb_U.st_size;

		fd_P = open("pos_vector", O_RDONLY);
		fstat(fd_P,&sb_P);
		size_t filesize_P = sb_P.st_size;

		int fd_I = open(inputfilename.c_str(),O_RDONLY);
		struct stat sb_I;
		fstat(fd_I,&sb_I);
		size_t filesize_I = sb_I.st_size;

		uint64_t *pos_vector = (uint64_t*)mmap(NULL, sb_P.st_size, PROT_READ, MAP_SHARED, fd_P, 0);
		uint64_t *U_vector = (uint64_t*)mmap(NULL, sb_U.st_size, PROT_READ, MAP_SHARED, fd_U, 0);

		uint64_t uncompressed_size = filesize_U/(sizeof(uint64_t));

		strings = j;
        coder.BLOCK = inserter->GetBlock();
		coder.Freeze(pos_vector,filesize,U_vector,uncompressed_size,outputfilename,strings);
	//	cout << "FREEZED" << endl;
		//This must contain the compression algorithm and the parameter
		string stat(outputfilename.c_str());
		stat = stat+"_stats";
		std::ofstream statsw(stat.c_str(),ios::binary);
		uint par = inserter->GetParameter();
		int Minblock = inserter->GetBlock();
		int buck = 0;
		if (bucket == true){
			buck ++;
		}
		//cout << "Minblock: " << Minblock << endl;
		statsw.write((const char*)&buck,sizeof(int));
		statsw.write((const char*)&par,sizeof(uint));
		statsw.write((const char*)&Minblock,sizeof(int));
		//cout << "writing " << j << endl;
		statsw.write((const char*)&j,sizeof(uint));
		statsw.close();

	}


	//Costruttore per caricare da file e fare rerieval
	CCOSBtree(std::string inputfilename,bool opt) {

		InputLoc = inputfilename;
		string EliasFanoLoc = inputfilename+"_Elias";
		string VvectorLoc = inputfilename+"_V_vector";
		string Stats = inputfilename+"_stats";
		string A_file = inputfilename+"_A_Vector";


		struct stat sb,sb_L,sb_S;
		int fd,fd_L,fd_S;

		fd = open(inputfilename.c_str(), O_RDONLY);
		fstat(fd, &sb);
		filesize = sb.st_size;

		fd_S = open(Stats.c_str(), O_RDONLY);
		fstat(fd_S,&sb_S);
		size_t filesize_S = sb_S.st_size;

		int* Alg = (int*)mmap(NULL, sb_S.st_size, PROT_READ, MAP_PRIVATE, fd_S, 0);
		coder.Algorithm = Alg[0];
		uint* par = (uint*)&Alg[1];
		coder.Parameter = *par;
		int Block = Alg[2];
		coder.BLOCK=Block;
		uint NUMS = (uint)Alg[3];
		//cout << "reading: " << NUMS << endl;
		coder.Num_string = NUMS;
		//cout << "setting c0der.block: " << Block << endl;
		

		//map the file to memory address
		addr = (char*)mmap(NULL, sb.st_size, PROT_READ, MAP_PRIVATE, fd, 0);
		coder.Set_addr(addr);
		MaxString = (char*)malloc(32000);

		coder.Map(EliasFanoLoc.c_str(),VvectorLoc.c_str(),A_file.c_str());
		NumCopied =  coder.num_Copied()-1;
		
		//My_ternary.Map(inputfilename+"_Ternary");


		if (opt){
			string LCPLocation = inputfilename+"_lcp";


			fd_L = open(LCPLocation.c_str(), O_RDONLY);
			fstat(fd_L, &sb_L);
			size_t lcp_size = sb_L.st_size;

			uint* lcp_addr = (uint*)mmap(NULL,sb_L.st_size, PROT_READ, MAP_SHARED, fd_L, 0);

			//coder.Set_Lcp(lcp_addr); 

		}


	}

	~CCOSBtree() {
		// TODO Auto-generated destructor stub
	}


uint64_t Get_Position_Size(){
	return coder.Ind.Lengths_size();
}

uint64_t Get_Indexes_size(){
	return coder.Ind.Indexes_size();
}

inline std::string Retrieval(const uint64_t u, const int l){
        
        int size;

        pair<byte*,uint64_t> infos = coder.Get(u);;
         
           uint64_t* st = (uint64_t*)infos.first;        
           uint64_t*sc = (uint64_t*)&(MaxString[0]);
        

            for (uint16_t addi = 0; addi < (l+7)>>3; addi++){
            	
                *sc = * st;
                sc ++;
                st ++;
            }


        for (uint i = 0; i<u-infos.second; i++){
        	                  
            st = (uint64_t*)coder.GetNext(&size);       
            sc = (uint64_t*)&MaxString[size];
            
            for (uint16_t addi = 0; addi < ((l-size)+7)>>3; addi++){ 
                *sc = * st;
                sc ++;
                st ++;
            }
         
		}
		return string(MaxString,l);
    }


	inline std::string Retrieval_Copied(uint64_t u,uint64_t* v,int P_size){
		
		*v = coder.Get_Index_of_Copied(u);

		uint64_t *st = (uint64_t*)coder.Get_F(u);;
		uint64_t *sc = (uint64_t*)&MaxString[0];

		// +7 "simulates" the ceiling
		uint16_t loop_l = min((unsigned long)&addr[coder.start]-(unsigned long)st,(unsigned long)P_size+1);

		for (uint16_t addi = 0; addi < (loop_l+7)>>3; addi++){	
			*sc = * st;	
			sc ++;
			st ++;
		}

		//string looked(MaxString,(unsigned long)&addr[coder.start]-(unsigned long)to_extract-coder.diff);
		//cout <<"looked: "<< looked << endl;
		return string(MaxString,loop_l);
	}

	inline std::string Retrieval_Copied(uint64_t u,int P_size){
		

		uint64_t *st = (uint64_t*)this->coder.Get_F(u);;
		uint64_t *sc = (uint64_t*)&this->MaxString[0];

		// +7 "simulates" the ceiling
		uint16_t loop_l = min((unsigned long)&this->addr[this->coder.start]-(unsigned long)st,(unsigned long)P_size+1);

		for (uint16_t addi = 0; addi < (loop_l+7)>>3; addi++){	
			*sc = * st;	
			sc ++;
			st ++;
		}

		//string looked(MaxString,(unsigned long)&addr[coder.start]-(unsigned long)to_extract-coder.diff);
		//cout <<"looked: "<< looked << endl;
		return string(this->MaxString,loop_l);

	}

	

	//uc is the index among the copied strings in which the block starts. end is the position among all the strings in which the block ends
	inline uint64_t ScanBlock(const string &P, uint64_t uc, uint64_t Last_string, bool second){
	
		uint64_t ret = coder.Get_Index_of_Copied(uc);
		uint16_t addi;
		int size,oldsize=0;
		uint64_t* st = (uint64_t*)coder.Get_F(uc);
	
		byte* end = coder.GetNext(&size);
		uint64_t* sc = (uint64_t*)&MaxString[0];
		// +7 "simulates" the ceiling
		uint siffix = (unsigned long)end-(unsigned long)st;

		for (addi = 0; addi < (siffix+7 )/8; addi++){	
			*sc = * st;	
			sc ++;
			st ++;
		}

		st = (uint64_t*)end;
		oldsize = size;

		//string looked = string(this->MaxString,oldsize+siffix);
		//cout << "During scan of block: " << looked << " "<< P<<endl;

		for (uint s=0; s< Last_string-ret-1+s; s++){
			ret ++;
			end = coder.GetNext(&size);
	
		sc = (uint64_t*)&MaxString[oldsize];
		// +7 "simulates" the ceiling

			siffix = (unsigned long)end-(unsigned long)st-coder.diff;
		for (addi = 0; addi <(siffix+7)/8 ; addi++){
			*sc = * st;
			sc ++;
			st ++;
		}
		//string looked = string(this->MaxString,oldsize+siffix);
		//cout << "During scan of block: " << looked << " "<< P<< " "<<P.size()<< " "<<oldsize+siffix<<endl;
	//}
		if (oldsize+siffix >= P.size() || second){
		//	cout << "comparing" << endl;
		if (!second){
		if (strcmp(P.c_str(),MaxString) <= 0){
		//	cout << "returning " << ret << endl;
			return ret;
		}
	}
	else {
		if (strcmp(P.c_str(),MaxString) <= 0){
			return ret;
		}
	}
}
	
		oldsize = size;
		st = (uint64_t*)end;
	
		}
		return ret+1;

	}

	//uc is the index among the copied strings in which the block starts. end is the position among all the strings in which the block ends
	inline void PScanBlock(const string &P, uint64_t uc, uint64_t Last_string,uint64_t* Range){
		size_t P_size = P.size();
		string PP(P);
		uint64_t ret = coder.Get_Index_of_Copied(uc);
		uint8_t searching_second=0;

		uint16_t addi;
		int size,oldsize=0;
		uint64_t* st = (uint64_t*)coder.Get_F(uc);
	
		byte* end = coder.GetNext(&size);
		uint64_t* sc = (uint64_t*)&MaxString[0];
		// +7 "simulates" the ceiling
		uint siffix = (unsigned long)end-(unsigned long)st;

		for (addi = 0; addi < (siffix+7 )/8; addi++){	
			*sc = * st;	
			sc ++;
			st ++;
		}

		st = (uint64_t*)end;
		oldsize = size;

		//string looked = string(this->MaxString,oldsize+siffix);
		//cout << "During scan of block: " << looked << " "<< PP<<endl;

		for (uint s=0; s< Last_string-ret-1+s; s++){
			ret ++;
			end = coder.GetNext(&size);
	
		sc = (uint64_t*)&MaxString[oldsize];
		// +7 "simulates" the ceiling

			siffix = (unsigned long)end-(unsigned long)st-coder.diff;
		for (addi = 0; addi <(siffix+7)/8 ; addi++){
			*sc = * st;
			sc ++;
			st ++;
		}
		//string looked = string(this->MaxString,oldsize+siffix);
		//cout << "During scan of block: " << looked << " "<< PP<< PP.size()<< " "<<oldsize+siffix<<endl;
	//}
		if (oldsize+siffix >= P_size || searching_second == 1){
		
		if (strcmp(PP.c_str(),MaxString) <= 0){
			Range[0+searching_second] = ret;
			if (searching_second == 0){
			searching_second ++;
			PP = PP+(char)0xFFFF;
		}
		else return;
		}
	}
	
		oldsize = size;
		st = (uint64_t*)end;
	
		}
		if (searching_second == 0){
			Range[0] = ret+1;
			Range[1] = ret+1;
		}
		else {
		 Range[0+searching_second] = ret+1;
		}

	}

	//uc is the index among the copied strings in which the block starts. end is the position among all the strings in which the block ends

 void Prefix_search( const string &P,uint64_t*Range){
			uint64_t LSI=0;
			
			Range[0] = ScanBlock(P,Prefix_search_LRI(P,1,NumCopied,&LSI)-1,LSI,false);

			string NP = P+(char)0xFFFF;
			Range[1] = ScanBlock(NP,Prefix_search_LRI(NP,1,NumCopied,&LSI)-1,LSI,true);
		
		}


	uint64_t Prefix_search_LRI(const string &P, uint64_t L, uint64_t R,uint64_t* v){
		uint64_t mid_Point = 0;
		int P_size = P.size();
		
		while (L!=R){
			mid_Point = (L+R)/2;

			switch(P.compare(Retrieval_Copied(mid_Point,v,P_size))){
				case 0 :
				return mid_Point;
				case 1 ... INT_MAX :
				L = mid_Point+1;
				break;
				case INT_MIN ... -1:
				R = mid_Point;
				break;
			}
		}
		*v = coder.Get_Index_of_Copied(L);
		return L;
	}	

	inline void FirstLevelS(const string &P){
		uint64_t LSI;
		Prefix_search_LRI(P,1,NumCopied,&LSI);
	}

	inline void FirstLevelP(const string &P){
		uint64_t LSI;
		Prefix_search_LRI(P,1,NumCopied,&LSI);
		string NP = P+(char)0xFF;
		Prefix_search_LRI(NP,1,NumCopied,&LSI);
	}

	uint64_t String_search(const string &P){
		uint64_t LSI = 0;
		return ScanBlock(P,Prefix_search_LRI(P,1,NumCopied,&LSI)-1,LSI,false);
	}



StringsCompressor* GetCompressor(){
	return inserter;
}

inline PATRICIA_TST Get_Tree(){
		return PATRICIA_TST();
	}

};

template <class CODER>
class Ternary_CCOSBtree : public CCOSBtree<CODER> {
public: 

	TST_Vector My_ternary;
	function<bool(const string&,const string&)> CompA = [] (const string& A, const string& B){
		return strcmp(A.c_str(),B.c_str()) <= 0;
	};
	function<bool(const string&,const string&)> CompB = [](const string& A, const string& B) {
		return strcmp(A.c_str(),B.c_str()) >= 0;
	};

	Ternary_CCOSBtree(uint c,uint d,bool bucket,std::string &inputfilename,string &outputfilename,bool opt) : CCOSBtree<CODER>(c,d,bucket,inputfilename,outputfilename,opt){
	//	cout << "size of copied: " << this->The_copieds.size() << endl;
		My_ternary.Insert(this->The_copieds);
		
		My_ternary.Purge_Leaves();
	
		My_ternary.freeze(outputfilename+"_Ternary");
	
	
	} 

	Ternary_CCOSBtree(std::string inputfilename,bool opt) : CCOSBtree<CODER>(inputfilename, opt){
		My_ternary.Map(inputfilename+"_Ternary");
	}

	


	inline void Prefix_search(const string &P, uint64_t* Range){
		bool M_at_leaf = false;
		pair<unsigned,unsigned> Pair = My_ternary.It_search(P.c_str(),&M_at_leaf);
		uint f = Pair.first;
		uint s = Pair.second;


		if (s < f){
			string M_comp = this->Retrieval_Copied(f,1000);
			switch (strcmp(M_comp.c_str(),P.c_str() )) {
				case 1 ... INT_MAX: {
					f--;
		
					if (std::mismatch(P.begin(), P.end(), M_comp.begin()).first != P.end())
					s --;
					break;
				}
				case 0 :{
					f--;
					break;
				}
				
			}
		}
		
	
			Range[0] = this->ScanBlock(P,f,this->coder.Get_Index_of_Copied(f+1),false);
	
			Range[1] = this->ScanBlock(P+(char)0xFFFF,s+1,this->coder.Get_Index_of_Copied(s+2),true);

	}

	uint64_t String_search(const string &P){
		bool M_at_leaf = false;
		pair<unsigned,unsigned> Pair = My_ternary.It_search(P.c_str(),&M_at_leaf);
		uint f = Pair.first;
		uint s = Pair.second;

		if (s < f){
			string M_comp = this->Retrieval_Copied(f,1000);
			switch (strcmp(M_comp.c_str(),P.c_str() )) {
				case 1 ... INT_MAX: {
					f--;
		
					if (std::mismatch(P.begin(), P.end(), M_comp.begin()).first != P.end())
					s --;
					break;
				}
				case 0 :{
					f--;
					break;
				}
				
			}
		}
	
			return this->ScanBlock(P,f,this->coder.Get_Index_of_Copied(f+1),false);

	}

inline PATRICIA_TST Get_Tree(){
		return PATRICIA_TST();
	}




};

template <class CODER>
class Patricia_CCOSBtree : public CCOSBtree<CODER> {
public: 

	PATRICIA_TST Tree;
	Patricia_pointer TreeP;
	

	Patricia_CCOSBtree(uint c,uint d,bool bucket,std::string &inputfilename,string &outputfilename,bool opt) : CCOSBtree<CODER>(c,d,bucket,inputfilename,outputfilename,opt){
	
		TreeP.Insert(this->The_copieds);
		PATRICIA_TST Vec_tree(TreeP);

		Vec_tree.freeze(outputfilename+"_Ternary");
		cout << "size of PATRICIA node: " << sizeof(PATRICIA_Node);
		cout << "TST OK" << endl;
		cout << "size of Trie: " << Vec_tree.size() << endl;
	
	} 

	Patricia_CCOSBtree(std::string inputfilename,bool opt) : CCOSBtree<CODER>(inputfilename, opt){
		Tree.Map(inputfilename+"_Ternary");
		
	}

	inline PATRICIA_TST Get_Tree(){
		return Tree;
	}

	inline void FirstLevelS(const string &P){
		pair<uint,uint> f = Tree.It_search(P.c_str(),P.size());
	}

	inline void FirstLevelP(const string &P){
		pair<uint,uint> f = Tree.It_search(P.c_str(),P.size());
	}


	inline void Prefix_search(const string &P, uint64_t* Range){
		pair<uint,uint> f = Tree.It_search(P.c_str(),P.size());
	
		string Fp1 = this->Retrieval_Copied(f.first+1,P.size()+1);

	
		uint CC = LcpS(Fp1.c_str(),P.c_str());
		//questo vuol dire che P è un prefisso delle stringhe copiate
		if (CC == P.size() ) {
					Range[0] = this->ScanBlock(P,f.first,this->coder.Get_Index_of_Copied(f.first+1),false);
					Range[1] = this->ScanBlock(P+(char)0xFF,f.second+1,this->coder.Get_Index_of_Copied(f.second+2),true);

			return;
		}

		uint i=0;
		while (Tree.Stack[i][1] <= CC){
				i++;
		}


		f = Tree.Tree[Tree.Stack[i][0]].Value;

		uint64_t GoC;
		if ((unsigned)Fp1[CC] > (unsigned)P[CC]){
			GoC = this->coder.Get_Index_of_Copied(f.first+1);
			this->PScanBlock(P,f.first,GoC,Range);
		}
		else {
			GoC = this->coder.Get_Index_of_Copied(f.second+2);
			this->PScanBlock(P,f.second+1,GoC,Range);
		}
	}

	inline uint64_t String_search(const string &P){
		
		pair<uint32_t,uint32_t> f = Tree.It_search(P.c_str(),P.size());

		string Fp1 = this->Retrieval_Copied(f.first+1,P.size()+1);
	
		uint CC = LcpS(Fp1.c_str(),P.c_str());
		//questo vuol dire che P è un prefisso delle stringhe copiate
		if ( CC == P.size() ) {
				return this->ScanBlock(P,f.first,this->coder.Get_Index_of_Copied(f.first+1),false);
		}

		uint i=0;
		while (Tree.Stack[i][1] <= CC){
				i++;
		}

		uint CN= Tree.Stack[i][0];

		f.first = Tree.Tree[CN].Value.first;
	
		f.second = Tree.Tree[CN].Value.second;

		if ((unsigned)Fp1[CC] > (unsigned)P[CC]){
	
			return this->ScanBlock(P,f.first,this->coder.Get_Index_of_Copied(f.first+1),false);
		}
		else {

			return this->ScanBlock(P,f.second+1,this->coder.Get_Index_of_Copied(f.second+2),false);
		}
	}
		
};







} /* namespace std */
#endif /* CCOSBTREE_H_ */






