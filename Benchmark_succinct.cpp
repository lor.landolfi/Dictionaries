#include "Benchmark_common.hpp"
#include "AllIncludes.h"
#include <boost/shared_ptr.hpp>
#include <boost/make_shared.hpp>
#include <vector>

using namespace std;

class Succinct_Benchmark {
public:
	virtual  ~Succinct_Benchmark() {};
	virtual void Prepare(const string& file_path,bool only_rank=false) =0;

};

class Succint_Select_only : public Succinct_Benchmark{
	

virtual void Prepare(const string& file_path,bool only_rank = false){
		succinct::elias_fano E;
		
		boost::iostreams::mapped_file_source m(file_path.c_str());
		size_t quindi = succinct::mapper::map<succinct::elias_fano>(E,m);
		uint64_t n_pos;
		uint64_t num_1 = E.num_ones();
		cout << "num1: " <<num_1 << endl;
		std::vector<uint64_t> v;
		v.reserve(num_1);
		srand(40);
		uint64_t tot = 0;
		for (int j=0; j<num_1; j++){
			v.push_back(rand()%num_1);
		}
		TIMEIT("Enumerator select on Elias_Fano", num_1*10){
			for (int j=0;j<10;j++){
				succinct::elias_fano::select_enumerator EN(E,0);
			for (uint64_t i=0; i<num_1; i++){
				//n_pos = E.select(v[i]);
				n_pos = EN.next();
				tot = tot + n_pos;
			}
		}
	}
	cout << tot << endl;
	tot = 0;

	TIMEIT("Random selects on Elias_Fano", num_1*10){
			for (int j=0;j<10;j++){
			for (uint64_t i=0; i<num_1; i++){
				//n_pos = E.select(v[i]);
				n_pos = E.select(v[i]);
				tot = tot + n_pos;
			}
		}
	}

	cout << tot << endl;

}

};


class Succinct_Rank_Select : public Succinct_Benchmark{
	

	virtual void Prepare(const string& file_path, bool only_rank = false){
		succinct::rs_bit_vector V;
		boost::iostreams::mapped_file_source m2(file_path.c_str());
		size_t dunque = succinct::mapper::map<succinct::rs_bit_vector>(V,m2);
		uint64_t V_ones = V.num_ones();
		uint64_t V_size = V.size();
		uint64_t tot = 0;
		std::vector<uint64_t> Random;
		Random.reserve(V_size);
		for (uint64_t i =0; i< V_size; i++){
			Random.push_back(rand()%V_size);
		}


			TIMEIT("Ranks on r_s bitvector consecutive", V_size*10){
			for (int j=0; j<10; j++){
			for (uint64_t i = 0; i< V_size; i++){
				tot = tot + V.rank(i);
				}
			}
		}
		cout << tot << endl;
	
		tot = 0;

		TIMEIT("Ranks on r_s bitvector random", V_size*10){
			for (int j=0; j<10; j++){
			for (uint64_t i = 0; i< V_size; i++){
				tot = tot + V.rank(Random[i]);
				}
			}
		}

		cout << tot << endl;
		Random.clear();
		Random.reserve(V_ones);

		for(uint64_t i=0; i<V_ones; i++){
			Random.push_back(rand()%V_ones);
		}

		TIMEIT("Selects on r_s bitvector consecutive", V_ones*10){
			for (int j=0; j<10; j++){
			for (uint64_t i = 0; i< V_ones; i++){
				tot = tot + V.select(i);
				}
			}
		}
		cout << tot << endl;
		tot = 0;

		TIMEIT("Selects on r_s bitvector random", V_ones*10){
			for (int j=0; j<10; j++){
			for (uint64_t i = 0; i< V_ones; i++){
				tot = tot + V.select(Random[i]);
				}
			}
		}
		cout << tot << endl;
	
	

	}
};

typedef std::map<std::string, boost::shared_ptr<Succinct_Benchmark> > benchmarks_type;

void print_benchmarks(benchmarks_type const& benchmarks)
{
    std::cerr << "Available benchmarks: " << std::endl;
    for (benchmarks_type::const_iterator iter = benchmarks.begin();
         iter != benchmarks.end();
         ++iter) {
        std::cerr << iter->first << std::endl;
    }
}

int main (int argc, char**argv){
	if (argc == 1 ){
		cout << "Integer decoding benchmark \n To use specify a binary file of unsigned integers " << endl;
		return 1;
	}

	
	using boost::shared_ptr;
    using boost::make_shared;

    benchmarks_type benchmarks;
    benchmarks["Elias_Fano"] = make_shared<Succint_Select_only>();
    benchmarks["Rank-Select"] = make_shared<Succinct_Rank_Select>();

    if (argc != 3 && argc != 4){
    	print_benchmarks(benchmarks);
    	return 1;
    }
    else {
    	const char *b = argv[1];
    	if (!benchmarks.count(b)) {
            std::cerr << "No benchmark " << b << std::endl;
            print_benchmarks(benchmarks);
            return 1;
        }
        else {
        	bool r=false;
        	if (argc == 4 )
        	r = atoi(argv[3]);
        	shared_ptr<Succinct_Benchmark> myptr = benchmarks[b];
        	cout << "Benchmark allocated " << endl;
        	myptr->Prepare(argv[2],r);
        }
    }
}


