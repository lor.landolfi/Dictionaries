#include "unaligned_io.hpp"

std::array<unsigned int, 9> unaligned_io::reader_masks = {0U, 1U, 3U, 7U, (1U << 4) - 1, (1U << 5) - 1, (1U << 6) - 1, (1U << 7) - 1, 0xFF};
