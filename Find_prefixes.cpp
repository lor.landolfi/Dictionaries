#include "AllIncludes.h"
#include <utility>
#include <functional>   // std::bind
#include <boost/accumulators/accumulators.hpp>
#include <boost/accumulators/statistics/stats.hpp>
#include <boost/accumulators/statistics/mean.hpp>
#include <boost/accumulators/statistics/moment.hpp>

using namespace std;

int main (int argc, char**argv){
    
    int remove;
    if (argc == 3){
        remove = atoi(argv[2]);
    }
    
	std::ifstream in(argv[1]);
	std::string line;
	string lastString ="";
	uint64_t i = 0;

	while (std::getline(in, line))
		{
			i++;
			auto res = std::mismatch(lastString.begin(), lastString.end(), line.begin());
            if (remove > 0){
                if (line[line.size()-1] == '!'){
                    line = line.substr(0,line.size()-1);
                }
                cout << line << endl;
            }
            else {
			if (res.first == lastString.end()){
				lastString.append("!");
			}
			if (i>1){
			cout << lastString << endl;
		}
			lastString = line;
            
		cout << line << endl;
        }
        }


}