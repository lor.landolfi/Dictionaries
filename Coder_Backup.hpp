/*template <class Indicizzatore>
class Unknown_lengths_coder {
public:

	uint* LCPADDRESS;
	Indicizzatore Ind;


	void Set_Lcp(uint* address){
		LCPADDRESS = address;
	}


	

	inline std::string Retrieval_Copied(uint64_t u,const char* addr, char* MaxString,uint64_t* v){
		//position of thr closest compressed string
		uint64_t stringvpos;
		//position of the last character of the closest not compressed string
		uint64_t end;
		//pointer to the character to copy from external memory
		uint64_t * st;
		//pointer to the position to write in the buffer
		uint64_t * sc;
		//pointer to the integers to be decoded
		byte * to_extract;
		
		*v = Ind.Get_Index_of_Copied(u);
		stringvpos = Ind.Get_position(u);

		
		end = Ind.GetNext();
		to_extract = (byte*)(&addr[stringvpos]);
		
		st = (uint64_t*)to_extract;
		sc = (uint64_t*)&MaxString[0];
		// +7 "simulates" the ceiling
		int loop_l = (end-stringvpos+7)/8;

		for (int addi = 0; addi < loop_l; addi++){	
			*sc = * st;	
			sc ++;
			st ++;
		}

		string looked(MaxString,end-stringvpos);
		//cout <<"looked: "<< looked << endl;
		return looked;

	}

	//uc is the index among the copied strings in which the block starts. end is the position among all the strings in which the block ends
	inline uint64_t ScanBlock(const string& P, uint64_t uc, uint64_t Last_string, const char* addr, char* MaxString){
		uint64_t start = Ind.Get_Index_of_Copied(uc);
		uint64_t stringvpos = Ind.Get_position(uc);
		uint64_t end = Ind.GetNext();
		uint64_t Max_scans = Last_string - start-1;
		uint size = 0;
		unsigned long diff = 0;
		int choice;
		uint64_t ret = start;
		uint16_t rearcoding;
		string looked;

		byte* to_extract = (byte*)(&addr[stringvpos]);
		uint64_t* st = (uint64_t*)to_extract;
		uint64_t* sc = (uint64_t*)&MaxString[size];
		// +7 "simulates" the ceiling
		int loop_l = ( end-stringvpos+7 )/8;
		for (int addi = 0; addi < loop_l; addi++){	
			*sc = * st;	
			sc ++;
			st ++;
		}
		size = end - (stringvpos);		
		stringvpos = end;

		for (uint s=0; s<Max_scans; s++){
			ret ++;
			end = Ind.GetNext();
			to_extract = (byte*)(&addr[stringvpos]);

			to_extract = IntegerEncoder.Decode(to_extract, &rearcoding);
			rearcoding = rearcoding -1;
			size = size - rearcoding;
			//to_extract now points to the first character of the string
		diff = (unsigned long)to_extract - (unsigned long)&addr[stringvpos];
		st = (uint64_t*)to_extract;
		sc = (uint64_t*)&MaxString[size];

		// +7 "simulates" the ceiling
		loop_l = (end-stringvpos-diff+7)/8;
			
		for (int addi = 0; addi < loop_l; addi++){
			*sc = * st;
			sc ++;
			st ++;
		}

		size = size + end - (stringvpos+diff);
		stringvpos = end;
		looked = string(MaxString,size);
		choice = P.compare(looked);
		if (choice <= 0){
			return ret;
		}
		}
		return ret+1;
	}

	


	std::vector<uint64_t> Prefix_search(const string &P,char* addr,char*buffer,const uint64_t strings,const succinct::cartesian_tree &t){


			std::vector<uint64_t> Range;
			uint64_t L=0;
			uint64_t R=strings-1;

		
			uint64_t Range1 = Prefix_search_LRI(P,L,R,addr,buffer,t);

			string NP(P);
			NP=NP+"~";
			Range.push_back(Range1);
		//	cout << "searching " << NP << endl;
			uint64_t Range2 = Prefix_search_LRI(NP.c_str(),Range1,R,addr,buffer,t);
		
			Range.push_back(Range2);
	
			return Range;

		}

		uint64_t Prefix_search_LRI(const string &P, uint64_t L, uint64_t R, char* addr, char* buffer,const succinct::cartesian_tree &t){
		uint64_t mid_Point = 0;
		string My_string;
		int choice;
		uint Max=0;
		string a = Retrieval(L,P.size()+1,addr,buffer);
		
		string b = Retrieval(R,P.size()+1,addr,buffer);
	
		uint l = LcpS(P,a);
	
		uint r = LcpS(P,b);
		
		uint m = 0;
		uint64_t Query_L;
		uint64_t Query_R;
		uint to_compare;
		while (L!=R){
			
			mid_Point = (L+R)/2;
			

			if(l == r ){
				My_string = Retrieval(mid_Point,P.size()+1,addr,buffer);
		
				choice = P.compare(l,string::npos,My_string,l,string::npos);
				m = l + LcpS(P,My_string,l,l);
			
				if (choice == 0){
		
				return mid_Point;
			}
			else if(choice < 0) {
				R = mid_Point;
				r = m;
			}
			else { 
				L = mid_Point+1;
				l = m;

				}
			}

			else{
				if (l > r) {
			
					Max = l;
					Query_L = L;
					Query_R = mid_Point;
				}
				else {
			
					Max = r;
					Query_L = mid_Point;
					Query_R = R;
				}

				to_compare = LCPADDRESS[t.rmq(Query_L+1,Query_R+1)];
			
				if (Max < to_compare){
				
				//	m = Max;
					if (Max == l)
					L = mid_Point +1;
					else 	
					R = mid_Point;
				}
				if (Max > to_compare){
		
			//		m = to_compare;
					if (Max == l)
					R = mid_Point;
					else 	
					L = mid_Point+1;
				}
				if (Max == to_compare){
			
					My_string = Retrieval(mid_Point,P.size()+1,addr,buffer);
			
					choice = P.compare(Max,string::npos,My_string,Max,string::npos);
					m = Max + LcpS(P,My_string,Max,Max);
					//m = LcpS(P,My_string);
		
				if (choice == 0){
				return mid_Point;
		
			}
			else if(choice < 0) {
				R = mid_Point;
				r = m;
			}
			else { 
				L = mid_Point+1;
				l = m;
				}
				}


			}	
	
		}

		return L;
	}	


	
		//retrieval using string.append
		inline std::string Slow_retrieval(uint64_t u, int l,char* addr,char* MaxString){
		

			uint64_t v;
			uint64_t stringvpos;
			uint64_t end;
			uint32_t rear;
			unsigned long diff = 0;
			string base ="";

			//get the index of the v-th string (the first one uncompressed)
			/*v = V.select(V.rank(u+1)-1);
			
			stringvpos = (E.select(v));*/
		/*	pair<uint64_t,uint64_t> infos = Ind.Get(u);
			stringvpos = infos.second;
			v = infos.first;
			
			byte * to_extract = (byte*)(&addr[stringvpos]);

			//succinct::elias_fano::select_enumerator SE (E,v+1);

			uint64_t loop_Times = u - v+ 1;

			for (uint i = 0; i<loop_Times; i++){

				//end = SE.next();
				end = Ind.GetNext();
				byte * to_extract = (byte*)(&addr[stringvpos]);

				if (base.size() > 0){
	
				to_extract = lzopt::hybrid::dst_decode(to_extract,&rear);
				rear = rear -2;
				base = base.substr(0,base.size()-rear);
				diff = (unsigned long)to_extract - (unsigned long)&addr[stringvpos];
			}

		
				base.append(&addr[stringvpos+diff],end-(stringvpos+diff));
				stringvpos = end;

			}
			return base.substr(0,l);

		}

};

template <class I,class V>
class Known_lengths_coder {
public:

	Unknown_lengths_coder<I,V> VB;
	V Ind;
	

	Known_lengths_coder() : VB() , Ind(){}

	void Set_Lcp(uint* address){
		VB.Set_Lcp(address);
	}

	

	inline std::string Retrieval_Copied(uint64_t u, const char* addr, char* MaxString, uint64_t* v){
		//position of thr closest compressed string
		uint64_t stringvpos;
		//pointer to the character to copy from external memory
		uint64_t * st;
		//pointer to the position to write in the buffer
		uint64_t * sc;
		//pointer to the integers to be decoded
		byte * to_extract;
		uint16_t suffix;
		
		*v = Ind.Get_Index_of_Copied(u);
		stringvpos = Ind.Get_position(u);

		to_extract = (byte*)(&addr[stringvpos]);
		to_extract = VB.IntegerEncoder.Decode(to_extract,&suffix);
      
        uint64_t end = stringvpos + suffix + ((unsigned long)to_extract-(unsigned long)&addr[stringvpos]);    

		
		st = (uint64_t*)to_extract;
		sc = (uint64_t*)&MaxString[0];
		// +7 "simulates" the ceiling
		int loop_l = (suffix+7)/8;

		for (int addi = 0; addi < loop_l; addi++){	
			*sc = * st;	
			sc ++;
			st ++;
		}	

		string looked(MaxString,suffix);
		//cout <<"looked: "<< looked << endl;
		return looked;

	}    

	//uc is the index among the copied strings in which the block starts. end is the position among all the strings in which the block ends
	inline uint64_t ScanBlock(const string& P, uint64_t uc, uint64_t Last_string, const char* addr, char* MaxString){
		//cout << uc << endl;
		if (uc > Last_string) {
			return 0;
		}
		uint64_t start = Ind.Get_Index_of_Copied(uc);
		//cout << "start: " << start << "Last "<< Last_string<<endl;
		uint64_t stringvpos = Ind.Get_position(uc);
		uint16_t suffix,rearcoding;
		uint64_t Max_scans = Last_string - start-1;
		uint size = 0;
		unsigned long diff = 0;
		int choice;
		uint64_t ret = start;

		byte* to_extract = (byte*)(&addr[stringvpos]);
		to_extract = VB.IntegerEncoder.Decode(to_extract,&suffix);
		uint64_t end = stringvpos + suffix + ((unsigned long)to_extract-(unsigned long)&addr[stringvpos]);  
		uint64_t* st = (uint64_t*)to_extract;
		uint64_t* sc = (uint64_t*)&MaxString[size];
		// +7 "simulates" the ceiling
		int loop_l = ( suffix+7 )/8;
		for (int addi = 0; addi < loop_l; addi++){	
			*sc = * st;	
			sc ++;
			st ++;
		}
		size = suffix;	
		//cout << string(MaxString,suffix) << endl;	
		stringvpos = end;

		for (uint s=0; s<Max_scans; s++){
			ret ++;
			
			to_extract = (byte*)(&addr[stringvpos]);
			to_extract = VB.IntegerEncoder.Decode(to_extract, &rearcoding);
			rearcoding = rearcoding -1;
			
			size = size - rearcoding;
			to_extract = VB.IntegerEncoder.Decode(to_extract,&suffix);
			
			
			//to_extract now points to the first character of the string
		diff = (unsigned long)to_extract - (unsigned long)&addr[stringvpos];
		end = stringvpos+suffix+diff;
		st = (uint64_t*)to_extract;
		sc = (uint64_t*)&MaxString[size];

		// +7 "simulates" the ceiling
		loop_l = (suffix+7)/8;
			
		for (int addi = 0; addi < loop_l; addi++){
			*sc = * st;
			sc ++;
			st ++;
		}

		size = size + suffix;
		stringvpos = end;
		string looked(MaxString,size);
		//cout << "looking: " << looked << endl;
		choice = P.compare(looked);
		//cout << "choice: " << choice <<endl;
		if (choice <= 0){
		//	cout << "returning " << ret << endl;
			return ret;
		}
		}
		return ret+1;
	}

	

    inline std::string Slow_retrieval(uint64_t u, uint l,char* addr,char* MaxString){
    	return "";
    }

    std::vector<uint64_t> Prefix_search(string &P,char* addr,char*buf){
   	return VB.Prefix_search(P,addr,buf); 
    }

    std::vector<uint64_t> Prefix_search(string &P,char* addr,char*buf,succinct::cartesian_tree &t){
   	return VB.Prefix_search(P,addr,buf,t); 
    }



};


template<typename encoding>
class GammaCoder {
public:

	unaligned_io::reader reader;
	unaligned_io::writer writer;
	byte LastByte;
	bool flushed;
	uint Offset;

	GammaCoder(byte* start): reader(start) , writer(start){
		//reader = new unaligned_io::reader(start);
		reader.bit_offset = 0;
		//writer = new unaligned_io::writer(start);
		writer.bit_offset = 0;
		flushed = false;
		LastByte = 0;
		Offset = 0;
	}

	uint32_t Insert (uint32_t to_encode,byte *write, string app,uint ilcp){

		uint32_t bits_inserted = 0;
		if (ilcp > 0 ){
			bits_inserted = gamma_like::encode<encoding>(to_encode+1,writer);
		}
		for (int var = 0; var < app.size()-ilcp; var ++) {
			writer.write((byte)app[var+ilcp],8);
		}
		//cout << "bit_offset "<<writer->bit_offset << endl;
		if (bits_inserted > 0)
			return bits_inserted + ((app.size()-ilcp)*8);
		else return ((app.size()-ilcp)<<3);
	}

	void Extract (uint64_t start, char* addr, uint64_t end,uint32_t *rear,string *base){
		//cout << "start and end: " << start << " " << end << endl;
		end = end >> 3;
		byte * to_Start =(byte*) &(addr[start >> 3]);
		//cout << (start >> 3) << " " << end << endl;
		reader.data = to_Start;
		//unaligned_io::reader reader(to_Start);
		reader.bit_offset = start % 8;
		//cout << "bit offset " << reader->bit_offset << endl;
		if (base->size() > 0 ){
			*rear = gamma_like::decode<encoding>(reader);
			*rear = *rear -1;
			//	cout << "decoded: " << *rear << endl;
			//	cout << "base size: " << base.size() << endl;
			*base = base->substr(0,base->size()-(*rear));
			//	cout << "base after shrinking: " << base << endl;
		}
		unsigned long diff = (unsigned long)&addr[(unsigned long)ceil((double)(end))] - (unsigned long) reader.data;
		unsigned char a, aca;
		/*if (reader->bit_offset > 0){
			end = end +8;
		}*/

	/*	for (int i=0; i<(diff); i++){
			a = (unsigned char)*reader.data;
			a = a >> reader.bit_offset;
			aca =(unsigned char) *(reader.data+1);
			aca = aca << (8-reader.bit_offset);
			a = a | aca;
			*base = *base +(char) a;
			reader.data = reader.data+1;
		}
		//return base;
	}

	void Flush(std::ostream * output, uint *index, unsigned char* buffer, uint64_t bufDim,bool final){
		uint i;
		//first time write to output stream from the first to the last byte -1
		//cout <<"flushing" <<endl;
		if (not flushed){
			flushed = true;
			i = 0;
			while(i < (uint64_t)ceil((double)(*index/8))){
				*output << buffer[i];
				i++;
			}
		}
		else {
			i = 1;
			//PrintBinary((char)LastByte);
			//PrintBinary((char)buffer[0]);
			//cout << Offset << endl;
			byte To_write = LastByte | buffer[0];
			*output << To_write;
			while(i < (uint64_t)ceil((double)(*index/8))){
				*output << buffer[i];
				i++;
			}
		}
		if (final){
			*output << buffer[(uint64_t)ceil((double)(*index/8))];
		}
		else{
			*index = 0;
			LastByte = *(writer.data);
			Offset = writer.bit_offset;
		//	cout << "Offset now: " << Offset << endl;
			memset(buffer,0,bufDim);
			writer.data = &buffer[0];
			//cout << "bit offset after flush "<< writer.bit_offset << endl;
		}
	}
};