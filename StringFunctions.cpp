#include "StringFunctions.h"

//Returns the longest common prefix between two strings as a unsigned int
uint Lcp2(const char*a,const  char*b, uint a_dim, uint b_dim){
	uint ind = 0;
	bool con = true;
	while (con && ind < std::min<uint>(a_dim,b_dim)){
		if (a[ind] == b[ind])
			ind = ind + 1;
		else
			con = false;
	}
	return ind;

}

uint LcpS( const string &a, const string &b,int off_a, int off_b){
	uint a_dim,b_dim;
	a_dim = a.size();
	b_dim = b.size();
	uint ind = 0;
	bool con = true;
	while (con && ind < std::min<uint>(a_dim-off_a,b_dim-off_b)){
		if (a[off_a + ind] == b[off_b + ind])
			ind = ind + 1;
		else
			con = false;
	}
	return ind;

}

//Emits the first lcpArray[a] characters of stringset[a]
std::string EmitString(uint64_t a, const std::vector<uint> &lcpArray, char **stringset, const std::vector<uint> &dim){
	uint l = lcpArray[a];
	std::string sub(stringset[a],l);
	return sub;
}

std::string EmitString(uint64_t a, const uint* lcpArray, succinct::elias_fano &E, char* f){
	uint l = lcpArray[a];
	uint64_t si;
	if (a>0)
	si = E.select(a-1);
	else
	si = 0; 
	//uint64_t si_end = E.select(a+1);
	std::string sub(&f[si],l);
	return sub;
}

//method used to simulate the visit DFS of a trie
void TrieVisitUtil(const uint * lcpArray, succinct::cartesian_tree *t,succinct::elias_fano &E,char *f,std::ofstream *output, uint64_t i,uint64_t j ){
	//cout << "i: "<< i << endl;
	//cout << "j: "<< j << endl;
	if(i == j){
		uint64_t si = E.select(i);
		uint64_t si_end = E.select(i+1);
		string a (&f[si],si_end-si-1) ;
		*output << a.append("$\n");
		//*output << lcpArray[i] << "\n";
		//cout << a << endl;
		//lpfc->Append(stringset[i]);
	}
	else {
		//find the index of the  minimum lcpArray
		uint64_t newi = t->rmq(i+1,j);
		//cout << "newi " << newi << endl;
		//emit the minimum longest common prefix

		if (lcpArray[newi] != 0 && lcpArray[newi] > lcpArray[i]){
			//cout << "emitting" << endl;
			std::string emit = EmitString(newi,lcpArray,E,f);
			*output << emit<< "\n";
			//cout << emit << endl;
			//lpfc->Append(emit);
		}
		TrieVisitUtil(lcpArray,t,E,f,output,i,newi-1);

		while(lcpArray[newi+1] == lcpArray[newi] && newi+1<=j){
			uint64_t si = E.select(newi);
			uint64_t si_end = E.select(newi+1);
			string a (&f[si],si_end-si-1);
			* output << a.append("$\n");
			//cout << a << endl;

			//lpfc->Append(stringset[newi]);
			newi = newi+1;
		}

		//call the method on the rest of the vector
		TrieVisitUtil(lcpArray,t,E,f,output,newi,j);
	}
}



//Simulate the visit of a compacted trie given a set of ordered strings and the lcp array
void TrieVisitSimulationSuccinct(char **stringset,const std::vector<uint> &dimensions, std::ofstream *output, const std::vector<uint> &lcpArray){
	succinct::cartesian_tree t (lcpArray);
	cout << "cartesian build " << endl;
	TrieVisitUtil(lcpArray,&t,stringset,dimensions,output,0,lcpArray.size()-2);
	//cout << "TrieVisitUtil done" << endl;
	//lpfc->Flush(true);
}

void TrieVisitSimulationSuccinct(succinct::elias_fano &E,char*f, std::ofstream *output, uint* lcpArray,uint64_t lcpArray_size){
	succinct::cartesian_tree::builder<uint> builder;
	cout << "size " <<  lcpArray_size << endl;
	cout << "Last position "  << E.select(lcpArray_size) << endl;
	for (uint64_t i=0; i<lcpArray_size; i++){
		builder.push_back(lcpArray[i],std::less<uint>());
		if (lcpArray[i] >= lcpArray_size ) {
			cout << lcpArray[i] << endl;
	}
	}
	cout << "builder done!" << endl;
	succinct::cartesian_tree t (&builder);
	cout << "cartesian build " << endl;
	TrieVisitUtil(lcpArray,&t,E,f,output,0,lcpArray_size-2);
	cout << "TrieVisitUtil done" << endl;
	//lpfc->Flush(true);
}

//method used to simulate the visit DFS of a trie
void TrieVisitUtil(const std::vector<uint> &lcpArray, succinct::cartesian_tree *t,char** stringset, const std::vector<uint> &dimensions,std::ofstream *output, uint64_t i,uint64_t j ){
	cout << "i: "<< i << endl;
	cout << "j: "<< j << endl;
	if(i == j){
		string a (stringset[i],dimensions[i]) ;
		*output << a;
		//cout << a << endl;
		//lpfc->Append(stringset[i]);
	}
	else {
		//find the index of the  minimum lcpArray
		uint64_t newi = t->rmq(i+1,j);
		//emit the minimum longest common prefix

		if (lcpArray[newi] != 0 && lcpArray[newi] > lcpArray[i]){
			//cout << "emitting" << endl;
			std::string emit = EmitString(newi,lcpArray,stringset,dimensions);
			*output << emit<< "\n";
			//cout << emit << endl;
			//lpfc->Append(emit);
		}
		TrieVisitUtil(lcpArray,t,stringset,dimensions,output,i,newi-1);

		while(lcpArray[newi+1] == lcpArray[newi] && newi+1<=j){
			string a (stringset[newi],dimensions[newi]);
			*output << a;
			//cout << a << endl;

			//lpfc->Append(stringset[newi]);
			newi = newi+1;
		}

		//call the method on the rest of the vector
		TrieVisitUtil(lcpArray,t,stringset,dimensions,output,newi,j);
	}
}



//Builds the LCP-Array from a vector of strings
uint64_t LCP_Builder (succinct::elias_fano &E, char* f, const string location){
	std::ofstream lcp_vector(location, ios::binary);
	uint64_t size = E.num_ones()-2;
	uint64_t si = E.select(0);
	uint64_t si_end = E.select(1);
	
	uint lcp=0;
	lcp_vector.write((const char*)&lcp,sizeof(uint));
	string first(&f[si],si_end-si);
	for (uint64_t i=1; i<size; i++){
		si = E.select(i);
		si_end = E.select(i+1);
		string second(&f[si],si_end-si);
		lcp = LcpS(first,second);
		//cout << "first: " << first << " second: " << second << endl;
		if (i<10){
		cout << "lcp: " << lcp << endl;
		cout << "first: " << first << " second: " << second << endl;
	}
		lcp_vector.write((const char*)&lcp,sizeof(uint));
		first = second;
	}
	lcp_vector.close();
	//cout << "LCPED" << endl;
	return size;
}

std::ifstream::pos_type filesize(const char* filename)
{
	std::ifstream in(filename, std::ifstream::in | std::ifstream::binary);
	in.seekg(0, std::ifstream::end);
	return in.tellg();
}







