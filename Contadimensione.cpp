#include "AllIncludes.h"
#include <utility>
#include <functional>   // std::bind
#include <boost/accumulators/accumulators.hpp>
#include <boost/accumulators/statistics/stats.hpp>
#include <boost/accumulators/statistics/mean.hpp>
#include <boost/accumulators/statistics/moment.hpp>



static const char alphanum[] =
"0123456789"
"ABCDEFGHIJKLMNOPQRSTUVWXYZ"
"abcdefghijklmnopqrstuvwxyz";

using namespace std;
using namespace boost::accumulators;

uint LcpS( const string &a, const string &b,const int off_a=0, const int off_b=0){
	uint a_dim,b_dim;
	a_dim = a.size();
	b_dim = b.size();
	uint ind = 0;
	bool con = true;
	while (con && ind < std::min<uint>(a_dim-off_a,b_dim-off_b)){
		if (a[off_a + ind] == b[off_b + ind])
			ind = ind + 1;
		else
			con = false;
	}
	return ind;

}

//first element: a value of values, second: number of occurrences of first in values
template <typename T>
void Get_distribution( std::vector<T> &values, std::vector<pair<T,uint> > &ret){
	sort(values.begin(),values.end());

	uint index = 1;
	short firstval = values[0];

	ret.push_back(pair<short,uint>(firstval,1));

	while (index < values.size()){
		while(values[index] == firstval){
		
			ret[ret.size()-1].second = ret[ret.size()-1].second +1;
			index ++;
			}

		firstval = values[index];
		ret.push_back(pair<short,uint>(firstval,1));
		index ++;
		}

}

template <typename T>
std::pair<double,double> Get_mean_and_variance(const std::vector<T> &values){
		std::pair<double,double> ret;
		 accumulator_set<double, stats<tag::mean, tag::moment<2> > > acc;
		for (uint i=0; i<values.size(); i++){
			if (values[i] > 0){
			acc(values[i]);
		}
		}

		ret.first = mean(acc);
		ret.second = sqrt(boost::accumulators::moment<2>(acc)-(ret.first*ret.first));
		//ret.second = sqrt(variance(acc));
		
		return ret;

}

	bool InsertCompress(int Alg, uint64_t scan,uint64_t string_size,uint64_t par, int cur_prefix, int strings){
		if (Alg == 1){
			if(scan > string_size*par || cur_prefix == 0){
				return false;
			}
			else {
				return true;
			}
		}
		else{
			if ((strings-1)%par == 0){
				return false;
			}
			else {
				return true;
			}
		}

	}






int main (int argc, char**argv){

		if (argc != 4 && argc !=3){
			cout << "Help: " << endl;
			cout << "srgument 1: file of strings , argument 2: parameter for LPFC" << endl;
		}

		std::ifstream in(argv[1]);
		std::string outputLoc(argv[1]);
		outputLoc = outputLoc+"_suffixes";
		string lastString ="";
		short current_prefix = 0;
		uint string_size = 0;
		short Algorithm = 0;

		
		std::string line;


		std::ofstream output(outputLoc.c_str(), ios::binary);


		if (string(argv[2]).compare("FC") == 0){
			Algorithm = 1;
		}
		if (string(argv[2]).compare("BC") == 0){
			Algorithm = 2; 
		}
		if (Algorithm == 0){
			cerr << "Not valid algorithm " << endl;
			return 0;
		}

		uint par = atoi(argv[3]);
		cout << "Algorithm: " << Algorithm << endl;
		std::vector<short> prefixes;
		std::vector<short> suffixes;
		std::vector<short> rears;
		std::vector<int> sizes;
		std::vector<uint> scanned_chars;
		std::vector<uint> scanned_strings;
		double Total_Length=0.0;
		uint64_t rear2=0;
		uint64_t suffix2=0;
		uint64_t totla_space = 0;
		uint64_t All_integers = 0;
		uint64_t Last_Copied_string = 0;
		short Uncompressed_seen = 0;

		
		uint scanned = 0;
		uint64_t strings = 0;
		uint64_t Uncompressed_ALL = 0;
		short compressed = 0;
		
		
		while (std::getline(in, line))
		{
			string_size = (line.size());
			sizes.push_back(string_size);
			Total_Length = Total_Length + (double)string_size;
			strings ++;

			current_prefix = (short)LcpS(lastString,line);
			//push prefix
			if (!InsertCompress(Algorithm,scanned,string_size,par,current_prefix,strings)){
			//cout << "s" << endl;
				Uncompressed_ALL ++;
				suffixes.push_back(string_size);
				output.write((const char*)&string_size,sizeof(short));
				All_integers++;
				scanned = string_size;
				scanned_chars.push_back(string_size);
				scanned_strings.push_back(1);
				Last_Copied_string = strings -1;
				compressed = 0;

				if (string_size > 63 ){
					suffix2 = suffix2 +1;
					totla_space = totla_space + 1;
				}
				totla_space ++;
			}
			else {
			prefixes.push_back(current_prefix);
			compressed ++;
		
			//push suffix
			suffixes.push_back(string_size - current_prefix);
			output.write((const char*)&suffixes[suffixes.size()-1],sizeof(short));
			totla_space = totla_space +2;
			//push rear
			rears.push_back(lastString.size() - current_prefix+1);
			output.write((const char*)&rears[rears.size()-1],sizeof(short));
			All_integers = All_integers +2;
			scanned = scanned + string_size - current_prefix;

			scanned_chars.push_back(scanned);
			scanned_strings.push_back(((strings-1) % par)+1);


		}

			lastString = line;

		}

		pair<double,double> prefixes_moments = Get_mean_and_variance<short>(prefixes);
		pair<double,double> suffixes_moments = Get_mean_and_variance<short>(suffixes);
		pair<double,double> rears_moments = Get_mean_and_variance<short>(rears);
		pair<double,double> chars_moments = Get_mean_and_variance<uint>(scanned_chars);
		pair<double,double> strings_moments = Get_mean_and_variance<uint>(scanned_strings);

		cerr << "Number of strings: " << strings << endl;
		cerr << "Number of integers: "<< All_integers << endl;
		cerr << "Prefixes mean: " << prefixes_moments.first << " variance: " << prefixes_moments.second << endl; 
		cerr << "Suffixes mean: " << suffixes_moments.first << " variance: " << suffixes_moments.second << endl; 
		cerr << "Rears mean: " << rears_moments.first << " variance: " << rears_moments.second << endl;
		cerr <<"Strings average length " << Total_Length/(double)strings << endl;
		cerr << "number of exceeding suffixes: " << suffix2 << endl;
		cerr << "number of exceeding rears: " << rear2 << endl; 
		cerr << "Total space: " << totla_space << endl;
		cerr << "minimum size: " << *std::min_element(sizes.begin(),sizes.end()) << endl;
		cerr << "maximum size: "<<*std::max_element(sizes.begin(),sizes.end())	<< endl;
		cerr << "Average scanned chars: " << chars_moments.first << endl;
		cerr << "Average scanned strings: " << strings_moments.first << endl;
		cerr << "Number of copied strings: " << Uncompressed_ALL << endl;

		//a questo punto nei vettori sono memorizzate tutte le informazioni, devo ordinarli e contare quante volte si ripete lo stesso numero
		std::vector<pair<short,uint> > prefixes_count;
		Get_distribution<short>(prefixes,prefixes_count);
		prefixes.clear();

		std::vector<pair<short,uint> > suffixes_count;
		Get_distribution<short>(suffixes,suffixes_count);
		suffixes.clear();

		std::vector<pair<short,uint> > rears_count;
		Get_distribution<short>(rears,rears_count);
		rears.clear();

		uint max_t = max(rears_count.size(),suffixes_count.size());
		uint max_it = max(max_t,(uint)prefixes_count.size());

	if (argc == 5){
		cout << "PREFIXES distribution " << "REARS distribution " << "SUFFIXES distribution " << endl;

		//at this point I have the distribution of all the data i'm interested in, must output them
	
		for (uint i=0; i< max_it; i++){
			if (i<prefixes_count.size())
				cout << prefixes_count[i].first << " " << prefixes_count[i].second <<" ";
			else
				cout << prefixes_count[prefixes_count.size()-1].first << " " << prefixes_count[prefixes_count.size()-1].second <<" ";
			if (i<rears_count.size())
				cout << rears_count[i].first << " " <<rears_count[i].second << " ";
			else
				cout << rears_count[rears_count.size()-1].first << " " << rears_count[rears_count.size()-1].second <<" ";
			if(i<suffixes_count.size())
				cout <<suffixes_count[i].first << " " <<suffixes_count[i].second << " ";
			else
				cout << suffixes_count[suffixes_count.size()-1].first << " " << suffixes_count[suffixes_count.size()-1].second <<" ";
			cout << endl;
		}
	}

	}
	

	