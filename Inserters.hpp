
#include "unaligned_io.hpp"

#include "StringsCompressor.h"
#include "StringFunctions.h"
#include "encoders.hpp"
#include "Indicizzatori.hpp"
#include <bitset>


using namespace std;

typedef uint8_t byte;
template <class INSERTER>
class LPCStructure : public StringsCompressor {


public:

	//If the "constant parameter" is 0 it implements simple front coding
	//Else it implements locality preserving front coding



	std::ostream *output;					//file where i put the result of the compression
	uint64_t bufDim;      						//size of the buffer
	unsigned char *buffer; 					//buffer to fetch and store the strings
	uint64_t C;									//parametric constant to tune the LPFC: no scan more than C characters
	uint64_t index;							//Current buffer index
	uint64_t TotalBytes;			
	uint64_t TotalBytesS;					//total number of bytes occupied by the strings compressed by the algorithm
	uint64_t TotalBytesI;    				//total number of bytes occupied by integer encoding
	uint64_t TotalBytesU;					//total number of bytes occupied by the uncompressed strings
	uint64_t NC;								//Number of characters to be scanned to decode the current string
	uint64_t lastindex;						//the incremental index independent from the buffer, useful for the positions vector
	string lastString;						//the last inserted string
	uint64_t currentStringNumber;   		//the number of strings currently stored;
	uint64_t copiedstrings;
	std::vector<uint64_t>  unconpressed; 	//positions of uncompressed strings
	std::vector<uint64_t> positions;		//positions where I put the strings in the buffer
    std::vector<uint> suffixes;
	INSERTER Ind;
	std::ofstream Pos_vector;				//the vector that will be encoded using Elias-Fano (had to put it in external memory because of large files)
	std::ofstream Uncompressed_vector;		//this one will become a rank-select vector
	bool AllPos;
	uint MINBLOCK=4000000000;
	int CURBLOCK=0;


	LPCStructure(uint constant, uint BufDimension, std::ostream *out) :Pos_vector("pos_vector", ios::binary), Uncompressed_vector("Uncompressed_vector",ios::binary) {
		// TODO Auto-generated constructor stub
		//output: output stream for the compressed file
		output=out;
		//C: parameter of the compression algorithm
		C=constant;
		//Parametric buffer size
		buffer = (unsigned char*) malloc(BufDimension);
		AllPos = Ind.AllPosition();
		
		bufDim = BufDimension;
		index = 0;
		NC = 0;
		TotalBytes = 0;
		TotalBytesS = 0;
		TotalBytesU = 0;
		TotalBytesI = 0;
		lastString = "";
		lastindex =0;
		currentStringNumber = 0;
		copiedstrings = 0;
	
	}


	//Append string s with REAR coding
	bool Append(string s){

		uint lcpLast = Lcp2(s.c_str(),lastString.c_str(),s.size(),lastString.size());
		return this->AppendLPFC(s,lcpLast);
	}

	void Flush(bool final){
		//U = uncompressed P= positions Pos_vector=PS Uncompressed_vector=US
		uint64_t i=0;
		while(i < index){
			*output << buffer[i];
			i++;
		}
		index = 0;
		memset(buffer,0,bufDim);
	
		if (positions.size() > 0)
		Pos_vector.write((const char*)&positions[0], positions.size()*sizeof(uint64_t));
		if (unconpressed.size() > 0)
		Uncompressed_vector.write((const char*)&unconpressed[0], unconpressed.size()*sizeof(uint64_t));
		unconpressed.clear();
		positions.clear();
		if (final){
			Pos_vector.close();
			Uncompressed_vector.close();
		}
		
	}
	


	void clear(){
		positions.clear();
		unconpressed.clear();
	}

	std::vector<uint64_t>* GetPositions(){
		return &positions;
	}
	std::vector<uint64_t>* GetUncompressed(){
		return &unconpressed;
	}
	uint64_t GetTotalBytesS(){
		return TotalBytesS;
	}
	uint64_t GetTotalBytesI(){
		return TotalBytesI;
	}
	uint64_t GetTotalBytesU(){
		return TotalBytesU;
	}
	uint64_t GetStringNumber(){
		return currentStringNumber;
	}
	uint64_t GetCopied(){
		return copiedstrings;
	}
	uint GetParameter(){
		return C;
	}

	void Clear(){
		free(buffer);
	}

	uint GetBlock(){
		return MINBLOCK;
	}

	string Get_Last_String(){
		return lastString;
	}

	bool AppendLPFC(string app, uint ilcp){

		uint16_t suffix = app.length()-ilcp;
		uint16_t rear =0;
		uint tag = 0;
		bool unc = false;
		//suffix for rear coding |string[i-1]|-lcp(string[i],string[i-1])
		rear = lastString.length()-ilcp;
		//add the index of the position of the currently stored string
		if (AllPos ==  true){
		positions.push_back(lastindex);
	}
		//check whether inserting the string I cross the buffer boundaries
		if (app.length()-ilcp+8+index+4 > bufDim ){
			Flush(false);
		
		}
			byte* to_insert = (byte*)&buffer[index];

		if ((((NC <= app.length()*C))|| C == 0 ))  {
			//if the string shares no characters with the previous one i have to insert it uncompressed anyway
			if (ilcp == 0){
				unconpressed.push_back(currentStringNumber);
				unc = true;
				if (AllPos == false)
					positions.push_back(lastindex);
				NC = 0;
				copiedstrings++;
			}
			if (ilcp > 0){
			tag = Ind.Insert(rear+1,to_insert);
			assert(rear +1 > 0);
		}
			
			if (AllPos == false){
				if (suffix == 0){
					cout << currentStringNumber << endl;
				}
				assert(suffix > 0);
				tag = tag + Ind.Insert(suffix,to_insert);
		
			}
		
			memcpy(to_insert,&app.c_str()[ilcp],app.size()-ilcp);
		
			//update the index and NC
			NC = NC + app.size()-ilcp;
			index = index + tag +((app.size()-ilcp));
			lastindex = lastindex +tag +((app.size()-ilcp));
			if (ilcp != 0){
				TotalBytesS = TotalBytesS + app.size()-ilcp;
			}
			else {
				TotalBytesU = TotalBytesU + app.size();
			}
			TotalBytesI = TotalBytesI + (tag);
		}
		//else insert it uncompressed
		else {
			if (AllPos == false)
				positions.push_back(lastindex);
			unconpressed.push_back(currentStringNumber);
			unc = true;
			copiedstrings++;
			if (AllPos == false){
				assert(suffix > 0);
				suffix = app.length();
				tag = Ind.Insert(suffix,to_insert);
			}
			memcpy(to_insert,&app.c_str()[0],app.size());
			NC = app.size();
			//Update the indexes
			index = index + tag +((app.size()));
			lastindex = lastindex +tag +((app.size()));
			TotalBytesU = TotalBytesU + app.size();
			TotalBytesI = TotalBytesI + (tag);
		}
		//update last string
		lastString = app;
		currentStringNumber++;
		if (!unc || currentStringNumber == 1 ){
			CURBLOCK ++;
		}
		else{
			if (CURBLOCK < MINBLOCK && currentStringNumber > 2){
				MINBLOCK = CURBLOCK;
				Ind.BLOCK = MINBLOCK;
				cout << "MINBLOCK: " << MINBLOCK << endl;
			}
			if (currentStringNumber > 1)
			CURBLOCK = 1;
		}
		return unc;

	}

void Append_LAST(){
	cout << "CALLED LAST" << endl;
	uint16_t suffix = lastString.length()+1;
	string LAST ="";
	/*for (int i = 0; i< suffix+1; i++){
		LAST = LAST+(char)0xFFFF;
	}*/
	LAST = lastString+(char)0xFFFF;
	cout << "last_string: " << LAST << endl;
	short tag = 0;
		if (AllPos ==  true){
			positions.push_back(lastindex);
		}
		if (LAST.length()+8+index > bufDim ){
			Flush(false);
		}

		byte* to_insert = (byte*)&buffer[index];
		unconpressed.push_back(currentStringNumber);

		if (AllPos == false){
				positions.push_back(lastindex);
				assert(suffix > 0);
				tag = tag + Ind.Insert(suffix,to_insert);
				}

		memcpy(to_insert,&LAST.c_str()[0],LAST.size());
		currentStringNumber++;
		TotalBytesU = TotalBytesU + LAST.size();
		TotalBytesI = TotalBytesI + (tag);
		copiedstrings++;

	}

};

template <class CODER>
class BucketCoder : public LPCStructure<CODER>  {
public:


	uint BucketSize;
	uint CurrentInBucket;

	BucketCoder(uint constant, uint BufDimension, std::ostream *out) : LPCStructure<CODER>(0,BufDimension,out) {
		
		BucketSize = constant;
		//this->C = 0;
		if (BucketSize > 0)
			CurrentInBucket = 0;
		else
			CurrentInBucket = 1;

	}


	//Append a string to output in bucket coding
	/*void Append(string s){
		//if I am at the first string of the Bucket, insert it uncompressed
		if (CurrentInBucket == 0)
			LPCStructure<CODER>::AppendLPFC(s,0,true);
		//else insert it compressed with standard front coding
		else
			LPCStructure<CODER>::Append(s,true);
		if (BucketSize > 0)
		CurrentInBucket = (CurrentInBucket +1)%BucketSize;
	}*/

	bool Append(string s){

		uint lcpLast = Lcp2(s.c_str(),this->lastString.c_str(),s.size(),this->lastString.size());
		return this->AppendLPFC(s,lcpLast);
	}

	bool AppendLPFC(string app, uint ilcp){

		uint16_t suffix = app.length()-ilcp;
		uint16_t rear =0;
		uint tag = 0;
		bool unc = false;
		//suffix for rear coding |string[i-1]|-lcp(string[i],string[i-1])
		rear = this->lastString.length()-ilcp;
		//add the index of the position of the currently stored string
		if (this->AllPos ==  true){
		this->positions.push_back(this->lastindex);
	}
		//check whether inserting the string I cross the buffer boundaries
		if (app.length()-ilcp+8+this->index+4 > this->bufDim ){
			this->Flush(false);
		}
			byte* to_insert = (byte*)&(this->buffer[this->index]);

		if (CurrentInBucket > 0 && this->currentStringNumber > 0)  {
			//if the string shares no characters with the previous one i have to insert it uncompressed anyway
			tag = this->Ind.Insert(rear+1,to_insert);
			assert(rear +1 > 0);
			
			if (this->AllPos == false){
				assert(suffix > 0);
				tag = tag + this->Ind.Insert(suffix,to_insert);
			}
			//tag = (unsigned long)to_insert - (unsigned long)&buffer[index];
			memcpy(to_insert,&app.c_str()[ilcp],app.size()-ilcp);
			//update the index and NC
			this->index = this->index + tag +((app.size()-ilcp));
			this->lastindex = this->lastindex +tag +((app.size()-ilcp));

			this->TotalBytesS = this->TotalBytesS + app.size()-ilcp;
			
			this->TotalBytesI = this->TotalBytesI + (tag);
		}
		//else insert it uncompressed
		else {
			if (this->AllPos == false)
				this->positions.push_back(this->lastindex);
			this->unconpressed.push_back(this->currentStringNumber);
			unc = true;
			this->copiedstrings++;
			if (this->AllPos == false){
				assert(suffix > 0);
				suffix = app.length();
				tag = this->Ind.Insert(suffix,to_insert);
			}
			memcpy(to_insert,&app.c_str()[0],app.size());
			//Update the indexes
			this->index = this->index + tag +((app.size()));
			this->lastindex = this->lastindex +tag +((app.size()));
			this->TotalBytesU = this->TotalBytesU + app.size();
			this->TotalBytesI = this->TotalBytesI + (tag);
		}
		//update last string
		this->lastString = app;
		this->currentStringNumber++;
		if (!unc || this->currentStringNumber == 1 ){
			this->CURBLOCK ++;
		}
		else{
			if (this->CURBLOCK < this->MINBLOCK && this->currentStringNumber > 1){
				this->MINBLOCK = this->CURBLOCK;
				this->Ind.BLOCK = this->MINBLOCK;
				cout << "MINBLOCK: " << this->MINBLOCK << endl;
			}
			if (this->currentStringNumber > 1)
			this->CURBLOCK = 1;
		}
		CurrentInBucket = (CurrentInBucket +1)%BucketSize;
		return unc;
	}



	uint GetParameter(){
		return BucketSize;
	}


};
//Encode methods returns the number of bits needed for encoding
class VBFast_Encoder{
public:

	VBFast_Encoder(){}

	 short Encode(uint16_t to_encode, byte* write){
		short tag =  lzopt::hybrid::len_encode(to_encode,write);
		return tag+1;
	}

	 byte* Decode(byte * read, std::uint16_t *value){
		byte* ret = lzopt::hybrid::len_decode(read,value);
		//*value = *value -2;
		return ret;
	}

};

class VBByte_Encoder{

public:

	VBByte_Encoder(){}

	 short Encode (uint16_t to_encode, byte* write){
	short tag = MSB::encodeVarint(to_encode,write);
	return tag;
	}

	 byte* Decode (byte *  read, uint16_t *value){
		
		*value= MSB::decodeVarint(read);
	
		return read;
	}



};

class Gamma_encoder{

public:
	unaligned_io::reader reader=NULL;
	unaligned_io::writer writer=NULL;
	short consecutive_in_byte=0;
	short old_bit_offset=0;
	byte* LastByte;
	short old_Tag=0;
	

	Gamma_encoder() {
		reader.data = (byte*)0;
		reader.bit_offset = 0;
		writer.data = (byte*)35647890;
		LastByte = 0;
	}	

	short Encode (uint16_t to_encode, byte* write){

		if(writer.data != write-1){
			writer.data=write;
			writer.bit_offset=0;
			consecutive_in_byte = 0;
			
		}
		if (consecutive_in_byte > 1 && writer.data == write-1){
			writer.data=write;
			writer.bit_offset=0;
			consecutive_in_byte = 0;
		
		
		}

		consecutive_in_byte++;
		old_bit_offset = writer.bit_offset;
		short tag = gamma_like::encode<soda09::soda09_len>(to_encode, writer);
		
		
		//tag = tag/8;
	
		
		if(consecutive_in_byte==1){
		return ceil((double)(tag)/8);
	}
	else{
		return ceil(((double)tag-(8-old_bit_offset))/8);
	}
	
	}


	 inline byte* Decode (byte *  read, uint16_t *value){
	 	
	 	if (read != LastByte){
	 		reader.data = read;
	 		reader.bit_offset = 0;
	 	} 
	 

	 	*value =gamma_like::decode<soda09::soda09_len>(reader);

	 	LastByte = reader.data + ((reader.bit_offset+7)>>3);
	 	return LastByte;
	 	

}

string GetName(){
	return "Gamma with padding";
}
		
};

//questa mette le posizioni di tutte le stringhe in elias-fano e gli indici in un array binario











