
#include "CCOSBtree.hpp"
#include <boost/shared_ptr.hpp>
#include <boost/make_shared.hpp>


using namespace std;

class benchmark
{
public:
    virtual ~benchmark() {}
    //questo deve mappare la struttura dati in tre file
    virtual int prepare(std::string benchmark_name, std::string strings_filename, std::string output_filename, std::vector<std::string> args) = 0;
    //questo testare le retrieval sulla struttuta dati
    virtual int measure(std::string benchmark_name, std::string filename, std::string filename_to_check,std::vector<std::string> args) = 0;
    //questo deve testare prefix search
   virtual int prefix_search(string benchmark_name, string filename, string filename_to_check, std::vector<string> args) = 0;
   //questo testa la ricerca di una stringa
   virtual int string_search(string benchmark_name, string filename, string filename_to_check, std::vector<string> args) = 0;
};

template<typename tree>
class benchmark_data_structure : public benchmark{

virtual int prepare(std::string benchmark_name, std::string strings_filename, std::string output_filename, std::vector<std::string> args){
	bool bucket = false;
	if (args[0].compare("BC") == 0){
		bucket = true;
	}
	uint constant = atoi(args[1].c_str());

	StringsCompressor* MyCompressor;

	TIMEIT(benchmark_name + " - construction", 1) {
            tree T(constant,20000000,bucket,strings_filename,output_filename,false);
            MyCompressor = T.GetCompressor();
        }

        int fd,fd_A,fd_T;
		size_t filesize,filesize_E,filesize_V,filesize_orig,filesize_A,filesize_T;
		//information of the file
		struct stat sb,sb_A,sb_T;

		fd = open(output_filename.c_str(),O_RDONLY);
		fstat(fd, &sb);
		filesize = sb.st_size;

		string EliasPath = output_filename+"_Elias";
		string V_Path = output_filename+"_V_vector";
		string A_Path = output_filename+"_A_Vector";
		string T_Path = output_filename+"_Ternary";

		fd = open(EliasPath.c_str(),O_RDONLY);
		fstat (fd,&sb);
		filesize_E = sb.st_size;

		fd = open(V_Path.c_str(),O_RDONLY);
		fstat (fd,&sb);
		filesize_V = sb.st_size;

		fd = open(strings_filename.c_str(),O_RDONLY);
		fstat(fd, &sb);
		filesize_orig = sb.st_size;

		fd_A = open(A_Path.c_str(),O_RDONLY);
		fstat (fd_A,&sb_A);
		filesize_A = sb_A.st_size;

		fd_T = open(T_Path.c_str(),O_RDONLY);
		fstat (fd_T,&sb_T);
		filesize_T = sb_T.st_size;


		string int_Size;
		string String_size;
		string Compressed_size;
		string Uncompressed_size;
		string NotCompressed_size;
		double Compression_ratio;

		bool LPFC = !bucket;

		Compression_ratio = ((double)(filesize+(filesize_V*(int)LPFC)+(filesize_A*(int)LPFC)+filesize_E+filesize_T)/(double)filesize_orig)*100;

		//cout << "SALG: " << SAlg << endl;

		int_Size = to_string(MyCompressor->GetTotalBytesI());
		Compressed_size = to_string(MyCompressor->GetTotalBytesS());
		Uncompressed_size = to_string(MyCompressor->GetTotalBytesU());
		String_size = to_string(MyCompressor->GetTotalBytesS() + MyCompressor->GetTotalBytesU());
		NotCompressed_size = to_string(filesize_orig);
		cerr << "copied strings: " << MyCompressor->GetCopied() << endl;
		cerr << "{"<< endl;
		cerr << "\"StringCompression\"  : \""+args[0]+"\" , \"parameter\" : \""+args[1]+"\", \"total file dimension\" : \" "+to_string(filesize+filesize_E+(filesize_V*(int)LPFC)+filesize_A*(int)LPFC+filesize_T)+"\", \"string dimension\" : \""
		+String_size+"\" , \"compressed strings \" : \""+Compressed_size+"\"  , \"uncompressed strings \" : \""+Uncompressed_size+"\" , \"integer dimension\" : \""+to_string(MyCompressor->GetTotalBytesI())+"\" , \"numstring\" : \""+to_string(MyCompressor->GetStringNumber())+
		"\" , \"Elias_Fano_dim\" : \""+to_string(filesize_E)+"\" , \"V_vector_dim\" : \""+to_string(filesize_V)+"\" , \"A_vector_dim\" : \""+to_string(filesize_A)+"\"Ternary dimension\" : \""+to_string(filesize_T)+"\"Original file dimension\" : \""+NotCompressed_size+"\", \"Compression ratio\" : \""+to_string(Compression_ratio)+"\""
		<<endl;
		cerr << "}"<<endl;

		return 0;


}

virtual int measure(std::string benchmark_name, std::string filename,std::string filename_to_check,std::vector<std::string> args){

	cout << benchmark_name << endl;
	
	tree T(filename_to_check,false);
	string b;

	succinct::util::mmap_lines strings_lines(filename.c_str());
    std::vector<std::string> strings(strings_lines.begin(), strings_lines.end());
    size_t m_size = strings.size();

    int strings_to_retrieve = atoi(args[0].c_str());
    std::vector<uint64_t> r;
   // r.reserve(strings_to_retrieve);
    srand(40);
    if (args.size() == 1){
   for (int i = 0; i< strings_to_retrieve; i++){
    	r.push_back((rand() * RAND_MAX + rand()) % m_size);
    }
}
/*	else {
		cout << "alternative" << endl;
		int temp = atoi(args[1].c_str());
		 for (int i = 0; i< strings_to_retrieve; i++){
		 //	cout << "will retrieve " << ((temp-1)*i)%m_size << endl;
    	r.push_back(((temp-1)*i)%m_size);
    }
	}*/
	else {
		int bound = atoi(args[1].c_str());
		for (uint j=0; j<strings.size(); j++){
			if (strings[j].size() < bound){
				r.push_back(j);
			}
		}
		random_shuffle(r.begin(),r.end());
	}
	cout << "retrieving " << r.size() << " elements " << endl;
 volatile size_t foo;
 strings_to_retrieve = r.size();
   cout << "check for correctness...";
   
    for (uint64_t i =0; i<m_size; i++){
				b = T.Retrieval(i+1,strings[i].size());
			//	cout << b << endl;
				/*if (b.compare(strings[i]) != 0){
				cout << strings[i] << endl;
				cout << b << endl;
				cout << "i: " << i << endl;
				}*/
				assert(b.compare(strings[i]) == 0);
				//BOOST_REQUIRE_EQUAL(b,strings[r[i]]);
			}
			cout <<"OK"<<endl;

   TIMEIT(benchmark_name+" consecutive retrievals TIME_ITERATION",m_size){
			for (uint64_t i =0; i<m_size; i++){
				b = T.Retrieval(i+1,strings[i].size());
			//	cout << b << endl;
			/*	if (b.compare(strings[i]) != 0){
				cout << strings[i] << endl;
				cout << b << endl;
				cout << "i: " << i << endl;
				}*/
				//assert(b.compare(strings[i]) == 0);
				//BOOST_REQUIRE_EQUAL(b,strings[r[i]]);
			}
		}

		TIMEIT(benchmark_name+" random retrievals TIME_ITERATION: ", strings_to_retrieve*10){
			for (int j=0; j<10; j++){
			for (uint64_t i= 0; i<strings_to_retrieve; i++){
				foo = T.Retrieval(r[i],strings[r[i]].size()).size();
			}
		}
		}


return 0;
}

virtual int prefix_search(string benchmark_name, string filename, string filename_to_check, std::vector<string> args){
	tree T(filename_to_check,false);
	//qui abbiamo le stringhe del file
	succinct::util::mmap_lines strings_lines(filename.c_str());
    std::vector<std::string> strings(strings_lines.begin(), strings_lines.end());

    size_t m_size = strings.size();


    int strings_to_retrieve = atoi(args[0].c_str());
    cout << "strings to retrieve: " << strings_to_retrieve << endl;
    std::vector<string> r;
   // r.reserve(strings_to_retrieve);
  
    srand(40);
   if (args.size() == 2){
   	int bound = atoi(args[1].c_str());
   	uint RN;
   for (int i = 0; i< strings_to_retrieve; i++){
   		RN = (rand() * RAND_MAX + rand()) % m_size;
   		if (strings[RN].size() > bound){
    	r.push_back(strings[RN].substr(0,bound));
    	// cout << r[i] << endl;
    }
    }  
}
else {
	cerr << "Incorrect number of arguments: " << endl;
	cerr << "Give: number of prefixes, length of prefixes " << endl;
	return 0;
}

uint64_t* Range = (uint64_t*)malloc(2*sizeof(uint64_t));

cout << "Check correctness....";

for (uint64_t i= 0; i<r.size(); i++){
			//	cout << "retrieving " <<r[i] << endl;
				T.Prefix_search(r[i],Range);
			//	cout << r[i].c_str() <<" " <<Range[0] << " "<< Range[1]<<" {" << endl;
				assert (Range[1] > Range[0]);
				for (uint64_t k=0; k< Range[1]-Range[0]; k++){
				//check if it is actually a prefix of all the strings in the returned range
				//cout << r[i] << "  " <<strings[Range[0]+k-1] << endl;
				auto res = std::mismatch(r[i].begin(), r[i].end(), strings[Range[0]+k-1].begin());
				//if (res.first != r[i].end())
				//	cout << r[i] << "  " <<strings[Range[0]+k] << endl;
				
				assert(res.first == r[i].end());
				//cout << strings[Range[0]+k] << " OK "<<endl;
			}
			auto res =  std::mismatch(r[i].begin(), r[i].end(), strings[Range[0]-1-1].begin());
			auto res2 =  std::mismatch(r[i].begin(), r[i].end(), strings[Range[1]-1].begin());
			if (res.first == r[i].end()){
			cout << "Range[0]: " << Range[0] << " Range[1]: " << Range[1] << endl;
			cout << strings[Range[0]-1-1] << " "<< r[i]<<endl << " }" << endl;
		}
			if (res2.first == r[i].end()){
				cout << "Range[0]: " << Range[0] << " Range[1]: " << Range[1] << endl;
			cout << strings[Range[1]-1] << " "<< r[i]<<endl << " }" << endl;
		}
			assert(res.first < r[i].end());
			assert(res2.first < r[i].end());
		}
	cout << "OK" << endl;

	TIMEIT(benchmark_name+" testing first level search: ", r.size()*5){
			for(int j=0; j<5; j++){
	for (uint64_t i= 0; i<r.size(); i++){
				T.FirstLevelP(r[i].c_str());
	
		}
		}
	}

   TIMEIT(benchmark_name+" random prefix searches TIME_ITERATION: ", r.size()*5){
			for (int j=0; j<5; j++){
			for (uint64_t i= 0; i<r.size(); i++){
				T.Prefix_search(r[i],Range);
		}
		}
		}
		return 0;
}

virtual int string_search(string benchmark_name, string filename, string filename_to_check, std::vector<string> args){
	tree T(filename_to_check,false);

	//qui abbiamo le stringhe del file
	succinct::util::mmap_lines strings_lines(filename.c_str());
    std::vector<std::string> strings(strings_lines.begin(), strings_lines.end());
    size_t m_size = strings.size();

    int strings_to_retrieve = atoi(args[0].c_str());
    cout << "strings to retrieve: " << strings_to_retrieve << endl;
    std::vector<string> r;
  
    srand(40);
  if (args.size() == 1){
  	uint RN;
   for (int i = 0; i< strings_to_retrieve; i++){
   		RN = (rand() * RAND_MAX + rand()) % m_size;
    	r.push_back(strings[RN]);
    	// cout << r[i] << endl;
    }  
}
else {
	cerr << "incorrect number of arguments , give number of strings to retrieve " << endl;
	return 0;
}


uint64_t Range;

cout << "Check correctness....";

for (uint64_t i= 0; i<m_size; i++){
			//	cout << "retrieving " <<strings[i] << endl;
				 if (i + 1 < strings.size()) {
                    succinct::intrinsics::prefetch(&strings[i + 1][0]);
                }
				Range = T.String_search(strings[i]);
				if (strcmp(strings[i].c_str(),strings[Range-1].c_str()) != 0){
					cout << strings[i] << " " << strings[Range-1] << endl;
				}
				assert(strcmp(strings[i].c_str(),strings[Range-1].c_str()) == 0);
		
			}
		
	cout << "OK" << endl;
	pair<uint,uint> f;
	uint64_t ss=0;
	//PATRICIA_TST m_tree = T.Get_Tree();

/*TIMEIT(benchmark_name+" testing First level search: ", m_size){
	for (uint64_t i= 0; i<m_size; i++){
			//	cout << "retrieving " <<strings[i] << endl;
				 if (i + 1 < m_size) {
                    succinct::intrinsics::prefetch(&strings[i + 1][0]);
                }
                T.FirstLevelS(strings[i].c_str());

			//	f = m_tree.It_search(strings[i].c_str(),strings[i].size());
			//	ss =  ss + f.second-f.first;
			//	cout << f[0] << " " << f[1] << endl;
			}
		}*/


 /*TIMEIT(benchmark_name+" consecutive searches TIME_ITERATION: ", m_size){
	for (uint64_t i= 0; i<m_size; i++){
			//	cout << "retrieving " <<strings[i] << endl;
				 if (i + 1 < m_size) {
                    succinct::intrinsics::prefetch(&strings[i + 1][0]);
                }
				Range = T.String_search(strings[i]);
			}
		}
*/
		TIMEIT(benchmark_name+" testing first level search: ", r.size()*5){
			for(int j=0; j<5; j++){
	for (uint64_t i= 0; i<r.size(); i++){
				T.FirstLevelS(r[i].c_str());
	
		}
		}
	}
		
		

    TIMEIT(benchmark_name+" random searches TIME_ITERATION: ", r.size()*5){
			for (int j=0; j<5; j++){
			for (uint64_t i= 0; i<r.size(); i++){
		
				Range = T.String_search(r[i]);
		}
		}
		}
		return 0;
}

};


typedef std::map<std::string, boost::shared_ptr<benchmark> > benchmarks_type;

void print_benchmarks(benchmarks_type const& benchmarks)
{
    std::cerr << "Available benchmarks: " << std::endl;
    for (benchmarks_type::const_iterator iter = benchmarks.begin();
         iter != benchmarks.end();
         ++iter) {
        std::cerr << iter->first << std::endl;
    }
}

int main(int argc, char** argv)
{
    using boost::shared_ptr;
    using boost::make_shared;

    benchmarks_type benchmarks;

     benchmarks["EliasAll+RS"] = make_shared<benchmark_data_structure<CCOSBtree<Succinct_All_Indexer<VBFast_Encoder> > > >();
     benchmarks["EliasCopied+RS+Gamma"] = make_shared<benchmark_data_structure<CCOSBtree<Succinct_Pos_Indexer<Gamma_encoder> > > >();
     benchmarks["EliasCopied+RS+VBFast"] = make_shared<benchmark_data_structure<CCOSBtree<Succinct_Pos_Indexer<VBFast_Encoder> > > > ();
     benchmarks["EliasCopied+Plain+VBFast"] = make_shared<benchmark_data_structure<CCOSBtree<Semi_Succinct<VBFast_Encoder> > > >();
     benchmarks["EliasCopied+Plain+Gamma"] = make_shared<benchmark_data_structure<CCOSBtree<Semi_Succinct<Gamma_encoder> > > >();
     benchmarks["EliasAll+Plain"] = make_shared<benchmark_data_structure<CCOSBtree<Semi_Succinct_All<VBFast_Encoder> > > >();
     benchmarks["AllPlain+VBFast"] = make_shared<benchmark_data_structure<CCOSBtree<Plain_Indexer<uint32_t,VBFast_Encoder> > > >();
     benchmarks["AllPlain+Gamma"] = make_shared<benchmark_data_structure<CCOSBtree<Plain_Indexer<uint32_t,Gamma_encoder> > > >();
     benchmarks["E_Plain+RS+VBFast"] = make_shared<benchmark_data_structure<CCOSBtree<E_plain_V_succinct<uint32_t,VBFast_Encoder> > > >();
     benchmarks["E_Plain+RS+Gamma"] = make_shared<benchmark_data_structure<CCOSBtree<E_plain_V_succinct<uint32_t,Gamma_encoder> > > >();
     benchmarks["AllPlain+A+Gamma"] = make_shared<benchmark_data_structure<CCOSBtree<Plain_with_A<uint32_t,Gamma_encoder> > > >();
     benchmarks["AllPlain+A+VBFast"] = make_shared<benchmark_data_structure<CCOSBtree<Plain_with_A<uint32_t,VBFast_Encoder> > > >();
    
     benchmarks["AllPlain+VBFast+Patricia"] = make_shared<benchmark_data_structure<Patricia_CCOSBtree<Plain_Indexer<uint32_t,VBFast_Encoder> > > >();
     benchmarks["AllPlain+Gamma+Patricia"] = make_shared<benchmark_data_structure<Patricia_CCOSBtree<Plain_Indexer<uint32_t,Gamma_encoder> > > >();


       if (argc == 1) {
        print_benchmarks(benchmarks);
        return 1;
    } else if (argc < 1 + 1 + 1 + 3) {
        std::cerr << "Missing arguments" << std::endl;
        return 1;
    } else {
        const char* b = argv[1];
        if (!benchmarks.count(b)) {
            std::cerr << "No benchmark " << b << std::endl;
            print_benchmarks(benchmarks);
            return 1;
        } else {
            shared_ptr<benchmark> inst = benchmarks[b];
            cerr << argv[1] << endl;
            if (std::string(argv[2]) == "prepare") {
                return inst->prepare(b, argv[3], argv[4], std::vector<std::string>(argv + 5, argv + argc));
            } else if (std::string(argv[2]) == "measure") {
                return inst->measure(b, argv[3], argv[4], std::vector<std::string>(argv + 5, argv + argc));
            } 
            else if (std::string(argv[2]) == "prefix"){
            	return inst->prefix_search(b, argv[3], argv[4], std::vector<std::string>(argv + 5, argv + argc));	
            }
             else if (std::string(argv[2]) == "search"){
            	return inst->string_search(b, argv[3], argv[4], std::vector<std::string>(argv + 5, argv + argc));	
            }
            else { 
                std::cerr << "Invalid command" << std::endl;
                return 1;
            }
        }
    }



}





