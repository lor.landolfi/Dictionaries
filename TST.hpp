

#include "AllIncludes.h"
//#include "StringFunctions.h"
#include "CCOSBtree.hpp"
#include <list>

using namespace std;
template <typename T>
void ffreeze(const string &FP,const char* L , size_t size){
	std::ofstream OS(FP, ios::binary);
	OS.write(L,sizeof(T)*size);
}

template <typename T>
void mmap(const string &FP, std::vector<T> &v){
	int fd = open(FP.c_str(), O_RDONLY);
	//cout << "opened " << endl;
	struct stat sb;
	fstat(fd, &sb);
	uint64_t filesize = sb.st_size;
		//map the file to memory address
		T* addr = (T*)mmap(NULL, sb.st_size, PROT_READ, MAP_SHARED, fd, 0);
		for (uint64_t j=0; j<filesize/sizeof(T);j++){
			v.push_back(addr[j]);
		}
		munmap(addr,filesize);
		
}


pair<unsigned,unsigned> Set_Pair(const char* s, int index, std::vector<string> &v,int imax,string MS){
//	cout << index << " " << imax << MS << endl;
	pair<unsigned,unsigned> P;
	int k=0;
	//cout << "testing if MS iterator works " << endl;

	/*if (MS.size() == 0){
		P.first = 0;
		P.second = v.size();
		return P;
	}*/

	while(index+k >=0 && std::mismatch(MS.begin(), MS.end(), v[index+k].begin()).first == MS.end() ){
		//cout << "confronting " << MS << " with " << v[index+k] << endl;
		k = k-1;
	}
	P.first= index+k+1;
	//cout << P.first << endl;
	k = 0;
	while(index+k <v.size() && std::mismatch(MS.begin(), MS.end(), v[index+k].begin()).first == MS.end()){
		//cout << "confronting " << MS<< " with " << v[index+k] << endl;
		k = k+1;
	}
	P.second = index + k -1;
	//cout << P.first << " " << P.second << endl;
	return P;
}

struct TST_Node2{

	char data;
	uint16_t left,right;
	pair<uint16_t,uint16_t> Value;

};


struct TST_Node
{
	char data;
	unsigned left,right;

	pair<unsigned,unsigned> Value;

	TST_Node(char data){
		this->data = data;
		left  = right  =0;

	}

	TST_Node(char data, pair<unsigned,unsigned> Vale) : TST_Node(data){
		Value = Vale;
	}

	bool Is_Equal(TST_Node &another){
		return (data == another.data && left == another.left && right == another.right && Value == another.Value);
	}


	friend ostream& operator<<(ostream& os, const TST_Node& dt){
    os <<'['<< dt.data <<   ',' << dt.left << ',' << dt.right << ']' <<" (" << dt.Value.first <<"," <<dt.Value.second <<")";
    return os;
}

};



class TST_Vector {
public:

	std::vector<TST_Node> Tree;


	void Insert(std::vector<string> &v){
		int i= 0;

		for (auto &s : v){
			s = s+"\n";
		}

		std::vector<uint> R(v.size());
		for (uint j=0; j<v.size(); ++j){
			R[j] = j;
			
		}
		random_shuffle(R.begin(),R.end());
		for (uint j=0; j<v.size();j++){
			Insert(0,v[R[j]].c_str(),R[j],1,v);
		}

	}

	// i può essere l'indice della stringa da inserire
	void Insert(uint CN,const char* word,int i,int imax, std::vector<string> &v){
	
		if (CN == Tree.size()){
			Tree.push_back(TST_Node(*word));
			Tree[CN].Value = Set_Pair(word,i,v,imax,string(word-imax+1,imax));
		}

		if ((uint)*word < (uint)Tree[CN].data){
			if (Tree[CN].left == 0){
				Tree[CN].left = Tree.size();
			}
			Insert(Tree[CN].left, word,i,imax,v);
		}

		else  if ((uint)*word > (uint)Tree[CN].data){

				if (Tree[CN].right == 0){
					Tree[CN].right = Tree.size();
		
				}
				Insert(Tree[CN].right,word,i,imax,v);
			}

		else {
			if (*(word+1)){
		
				Insert(++CN,word+1,i,++imax,v);
			}
		}

	}

void Purge_Leaves(){
	std::vector<TST_Node> N_Tree;
	uint foglie_tolte =0;
	uint k,start,end=0;
	start = 0;
	std::vector<uint> Hash;
	Hash.reserve(Tree.size());
	Hash[0] = 0;

	for (uint j=0; j< Tree.size(); j++){
		if (Tree[j].data == '\n'){
			k = j;
			while(Tree[k-1].Value.first == Tree[k].Value.first && Tree[k-1].Value.second == Tree[k].Value.second){
				
				k = k-1;

			}
			end = end -(j-k);
		
			for (int i=start; i<=end; i++){
				N_Tree.push_back(Tree[i]);
				Hash[i] = N_Tree.size()-1;
			}
			N_Tree.push_back(Tree[j]);
			Hash[end+1] = N_Tree.size()-1;
			
		foglie_tolte = foglie_tolte + (j-k);
		start = j+1;
		end = start;
	
			
	}
	else {
		end ++;
	}
	
	}

	for (auto &n : N_Tree){
		n.left = Hash[n.left];
		n.right = Hash[n.right];
	}

	swap (Tree,N_Tree);

}



pair <unsigned,unsigned> It_search(const char *word,bool* leaf){
	uint CN = 0;
	uint i = 0;
	while (true){
		//	cout << "word: " <<word[i]<<" "<< (uint)word[i] << " Data: " << Tree[CN].data<<" "<<(uint)Tree[CN].data;
		//	cout << "(" << Tree[CN].Value.first << " , " << Tree[CN].Value.second << ")" << endl;
		 if ((uint16_t)word[i] < (uint16_t)Tree[CN].data){
    		if(Tree[CN].left != 0){
        	CN = Tree[CN].left;
        }
        else {
        	if (Tree[CN].Value.first == Tree[CN].Value.second){
        		*leaf = true;
        	}
        	return pair<unsigned,unsigned>(Tree[CN].Value.first,Tree[CN].Value.first-1);
        }
    }
     else if ((uint16_t)word[i] > (uint16_t)Tree[CN].data){
    	if(Tree[CN].right != 0)
        	CN = Tree[CN].right;
        else {
        	if (Tree[CN].Value.first == Tree[CN].Value.second){
        		*leaf = true;
        	}
        	return pair<unsigned,unsigned>(Tree[CN].Value.second+1,Tree[CN].Value.second);
        }
    }
     else
    {	

        if (word[i+1] == '\0'){
            return Tree[CN].Value;
        }
        	
        	CN = CN +1;
        	i = i+1;

    }

	}
}


void Print(){
	int i = 0;
	for (auto &s : Tree){
		cout <<i <<":	"<< s << endl;
		i = i+1;
	}
}

size_t size(){
	return Tree.size();
}

double Percentage_of_Unary(){
	uint unary = 0;
	for (auto &s : Tree){
		if (s.left == 0 && s.right == 0 && s.data != '\n'){
			unary = unary + 1;
		}
	}
	return (double)unary/(double)Tree.size();
}

uint64_t size_without_chars (){
	return Tree.size()*sizeof(TST_Node) - Tree.size()*sizeof(pair<uint,uint>);
}



void freeze(const string &FP){
	ffreeze <TST_Node> (FP, (const char*)&Tree[0],Tree.size());
}

void Map(const string &FP){
	mmap<TST_Node> (FP,Tree);
}


};

struct PATRICIA_Node
{	
	pair<uint32_t,uint32_t> Value;
	uint32_t left,right;
	uint16_t length;
	unsigned char data;
	

	PATRICIA_Node(unsigned char data){
		this->data = data;
		left  = right  =0;
	
	}

	PATRICIA_Node(char16_t data, pair<unsigned,unsigned> Vale) : PATRICIA_Node(data){
		Value = Vale;
	}

	PATRICIA_Node(string &s) {
		length = s.size();
		data =(unsigned char) s[s.size()-1];
		left = right  = 0;
	}

	PATRICIA_Node(const PATRICIA_Node &other){
		data = other.data;
		left = other.left;
		right = other.right;
		length = other.length;
		Value = other.Value;
		
	}

	bool Is_Equal(TST_Node &another){
		return (data == another.data && left == another.left && right == another.right && Value == another.Value);
	}


	friend ostream& operator<<(ostream& os, const PATRICIA_Node& dt){
    os <<'['<< dt.data << ','<<dt.length  <<',' << dt.left << ',' <<dt.right<< ']' <<" (" << dt.Value.first <<"," <<dt.Value.second <<")";
    return os;
}

};



struct Patricia_Node {

	unsigned char data;
	uint16_t length;
	pair<unsigned,unsigned> Value;
	Patricia_Node* left;
	Patricia_Node* center;
	Patricia_Node*right;

	Patricia_Node(){}

	Patricia_Node(const string &s) {
		length = s.size();
		data = (unsigned char)s[length-1];
		left = right  = center =  0;
	}

	Patricia_Node(const Patricia_Node &other){
		data = other.data;
		left = other.left;
		right = other.right;
		length = other.length;
		Value = other.Value;
		center = other.center;
		
	}

	friend ostream& operator<<(ostream& os, const Patricia_Node& dt){
    os  <<" ["<<" (" << dt.Value.first <<"," <<dt.Value.second <<")"<< dt.data << ','<<dt.length  <<',' << dt.left << ',' << dt.center <<','<<dt.right<< ']';
    return os;
}

};

struct Patricia_pointer {

	Patricia_Node *root;

	Patricia_pointer() : root(){}


	void Insert (Patricia_Node **N,int i, std::vector<string> &v){
		//cout << "inserting " << v[i] << endl;
		if (*N == 0){
			*N = new Patricia_Node(v[i]);
			Patricia_Node *NN = *N;
			NN->Value = Set_Pair(v[i].c_str(),i,v,v[i].size(),string(v[i].c_str(),v[i].size()));
		}
		else {
		Patricia_Node * NN = *N;
		uint CC = LcpS(v[i].c_str(),v[NN->Value.first].substr(0,NN->length),0,0);	
		//cout << "CC calculated " << NN->length << endl;
		if (CC < NN->length){
			Patricia_Node *Tmp = new Patricia_Node(*NN);
			NN->length = CC;
			NN->data = (unsigned char)v[NN->Value.first].at(CC);

			NN->Value = Set_Pair(v[NN->Value.first].c_str(),NN->Value.first,v,CC,string(v[NN->Value.first].c_str(),CC));
		//	cout << "set pair in middle " << endl;
			NN->left= 0;
			NN->right = 0;
			NN->center = Tmp;

		}
			if ((unsigned char)v[i].at(CC) > NN->data ){
				Insert(&NN->right,i,v);
			}
			else if ((unsigned char)v[i].at(CC) < NN->data ){
				Insert(&NN->left,i,v);
			}
	
		else {
			Insert(&NN->center,i,v);
		}
	}
}

//Random tree generation
/*void Insert(std::vector<string> &v){
		int i= 0;

		for (auto &s : v){
			s = s+"\n";
		}

		std::vector<uint> R(v.size());
		for (uint j=0; j<v.size(); ++j){
			R[j] = j;
		}

		random_shuffle(R.begin(),R.end());
		
		for (uint j=0; j<v.size();j++){
			Insert(&root,R[j],v);
			
		}
	}
*/
//Tournament trie generation
	void Insert (std::vector<string> &v, int i, int j){
	//	cout << i << " " << j << endl;
		if ( j >= i){
		int m = (i+j)/2;
	//	cout << "inserting " << v[m] << v.size()<< " "<<m <<endl;
		Insert(&root,m,v);
	//	cout << "inserted " << endl;
		
		Insert(v,i,m-1);
		
		Insert(v,m+1,j);
}
	
	}


	void Insert(std::vector<string> &v){
		for (auto &s : v){
			s = s+"\n";
		}
		Insert(v,0,v.size()-1);
	}

	/*void InOrderTraversal(Patricia_Node* N,list<Patricia_Node*> &List){
		if (N != 0){
			InOrderTraversal(N->left,List);
			List.insert(N);
			InOrderTraversal(N->right,List);
		}
	}

	void RemoveLRChildren(list<Patricia_Node*> &List){
		for (auto &s : List){
			s->left = 0;
			s->right = 0;
		}
	}

	void Bin_Insert(Patricia_Node* N,Patricia_Node*Start){
		if 	
	}
	
	Patricia_Node* BalanceBinTree(uint min, uint max,list<Patricia_Node*> &List,Patricia_Node* Start){
		if (min <= max){
			uint Middle = ceil((double)(min+max)/2);
			Bin_Insert(List[Middle],Start);
			BalanceBinTree(min,Middle-1,List);
			BalanceBinTree(Middle+1,max,List);
			return List[Middle];
		}
	}

	Patricia_Node* Balance(Patricia_Node* N,Patricia_Node* Start){
		list<Patricia_Node*> L;
		InOrderTraversal(N,L);
		RemoveLRChildren(L);
		return BalanceBinTree(0,L.size()-1,L,Start);
	}

	Make_Balanced(Patricia_Node* Root, Patricia_Node* Father){
		if (Root != 0){

			Father->center = 0;
			Patricia_Node* NN = Balance(Root,Father);
			Father->center = NN;
			
			Make_Balanced(NN->center,Root);
			Make_Balanced(NN->left->center,NN->left);
			Make_Balanced(NN->right->center,NN->right);
		}
	}

	void Balance_All(){
		Patricia_Node N;
		N.center = this->root;
		Make_Balanced(this->root,&N);
		this->root = N->center;
	}*/

	void Print (){
		Print (root);
	}

	void Print (Patricia_Node *N){
		if (N!=0){
			cout << *N << endl; 
		}
		if (N->center != 0)
			Print(N->center);
		if (N->left != 0)
			Print(N->left);
		if (N->right != 0)
			Print(N->right);

	}
	

};

struct PATRICIA_TST {

	std::vector<PATRICIA_Node> Tree;
	//std::vector<pair<uint,uint> > Stack;
	uint Stack[128][2];

	void Transform (Patricia_Node *root,std::vector<PATRICIA_Node> &v){
	
		PATRICIA_Node c (root->data,root->Value);
		c.length = root->length;
		v.push_back(c);
		int M_index = v.size()-1;
		if (root->center != 0 && root->right == 0 && root->left == 0){
			cout << "there exists a unary node " << endl;
		}
		if (root->center != 0){
			Transform(root->center,v);
		}

		if (root->left != 0){
			v[M_index].left = v.size();
			Transform(root->left,v);
		}
		else {v[M_index].left = 0;}

		if (root->right != 0){
			v[M_index].right = v.size();
			Transform(root->right,v);
		}
		else {v[M_index].right = 0;}
	}

	PATRICIA_TST (Patricia_pointer &other){
		Transform(other.root, Tree);
		//std::vector<pair<uint,uint> > U(Tree.size());
		//Stack.reserve(Tree.size());
	}

	PATRICIA_TST (){}

	size_t size(){
		return Tree.size();
	}


	inline pair <uint,uint> &It_search(const char* word,size_t len){
	uint CN= 0;uint i=0;
	while (Tree[CN].length < len){
		//Stack.push_back(pair<uint,uint>(CN,Tree[CN].length));
		Stack[i][0] = CN;
		Stack[i++][1] = Tree[CN].length;
	//	cout << "word: " <<word[Tree[CN].length]<<" " << (wchar_t)word[Tree[CN].length] <<" Data: " << Tree[CN].data<<" ";
	//		cout << "(" << Tree[CN].Value.first << " , " << Tree[CN].Value.second << ")" << endl;

	//		cout << (unsigned char)word[Tree[CN].length] - Tree[CN].data << endl;
	//		cout << "CN " << CN << endl;
		switch ((unsigned char)word[Tree[CN].length] - Tree[CN].data){

			case INT_MIN ... -1:
					if(Tree[CN].left != 0){
        				CN = Tree[CN].left;
        				}
       			 else {
        				if (Tree[CN].Value.first != Tree[CN].Value.second)  { 
        			//	cout << "pushing! " << CN+1 << " " << Tree[CN+1].Value.first << endl;
        				//Stack.push_back(pair<uint,uint>(CN+1,Tree[CN+1].length)); 
        				Stack[i][0] = CN+1;
						Stack[i++][1] = Tree[CN+1].length;
					//	cout << "returning " << Tree[CN+1].Value.first << " " << Tree[CN+1].Value.second << endl;
        				return (Tree[CN+1].Value);
        			}
        				else         				
        				return (Tree[CN].Value);
       				 }
       				 break;

				case 1 ... INT_MAX:
					if(Tree[CN].right != 0)
        				CN = Tree[CN].right;
       				 else {
        				if (Tree[CN].Value.first != Tree[CN].Value.second){
        				//	cout << "pushing?? " << CN+1 << " " << Tree[CN+1].Value.first << endl;
        				//	Stack.push_back(pair<uint,uint>(CN+1,Tree[CN+1].length));
        				//	cout << "returning " << Tree[CN+1].Value.first << " " << Tree[CN+1].Value.second << endl;
        					Stack[i][0] = CN+1;
							Stack[i++][1] = Tree[CN+1].length;
        				return (Tree[CN+1].Value);
        			}
        				else 
        				return (Tree[CN].Value);
        			}
        			break;

    				case 0 :
    					CN = CN +1;
    					break;
    				
				}
			}
		//	cout << "returning " << Tree[CN].Value.first <<" "<<Tree[CN].Value.second<< endl;
		//	Stack.push_back(pair<uint,uint>(CN,Tree[CN].length));
			Stack[i][0] = CN;
			Stack[i++][1] = Tree[CN].length;
			return (Tree[CN].Value);
		}

		

	
	void Print(){
	int i = 0;
	for (auto &s : Tree){
		cout <<i <<":	"<< s << endl;
		i = i+1;
	}

}

uint ContaCopie(){
	std::vector<pair<uint32_t,uint32_t> > v;
	ContaCopieUtil(0,v);
	cout << endl;
	for (auto &s : v){
		cout << s.first << " " <<s.second  << endl;
	}
	return v.size();
}

void ContaCopieUtil(int ind, std::vector<pair<uint32_t,uint32_t> > &v){
	v.push_back(Tree[ind].Value);
	int n_ind = ind;
	while(Tree[n_ind].left != 0){
		if (Tree[Tree[n_ind].left].length > Tree[n_ind].length){
			ContaCopieUtil(Tree[n_ind].left,v);
		}
		else {
			ContaCopieUtil(Tree[n_ind].left+1,v);
		}
		n_ind = Tree[n_ind].left;
	}
	n_ind = ind;
	while (Tree[n_ind].right != 0){
		if (Tree[Tree[n_ind].right].length > Tree[n_ind].length){
			ContaCopieUtil(Tree[n_ind].right,v);
		}
		else {
			ContaCopieUtil(Tree[n_ind].right+1,v);
		}
		n_ind = Tree[n_ind].right;
	}

	if (Tree[ind].left !=0 || Tree[ind].right != 0){
		ContaCopieUtil(ind+1,v);
	}
}

	
void freeze(const string &FP){
	ffreeze<PATRICIA_Node> (FP, (const char*) &Tree[0] , Tree.size());
}

void Map(const string &FP){
	mmap<PATRICIA_Node> (FP,Tree);
	//for(int i=0; i<128; i++ ){
	//	Stack.push_back(pair<uint,uint>(0,0));
	//}
}


};


















