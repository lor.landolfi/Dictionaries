
#include "StringFunctions.h"
 #include <sys/mman.h>

int main (int argc, char**argv){


		std::ifstream in(argv[1]);
		std::string tried (argv[1]);
		tried = tried + "_tried";
		std::string lcp_loc(argv[1]);
		lcp_loc = lcp_loc.append("_lcp");
		std::ofstream out(tried.c_str());

		//Vector of strings storing the stringset in the input file
		std::string line;

		uint64_t j=0;
		while (std::getline(in, line))
		{
			//std::istringstream iss(line);
			//stringset2.push_back(line+"\"");
			j++;
		}

		//aggiustare MAP!!!!
		cout << "lines counted " << endl;

		int fd;
		size_t filesize;
		struct stat sb;
		fd = open(argv[1],O_RDONLY);
		fstat(fd, &sb);
		filesize = sb.st_size;
	
		char *file = (char*)mmap(NULL, sb.st_size, PROT_READ, MAP_SHARED, fd, 0);

		cout << "file mapped " << endl;
		
		succinct::elias_fano::elias_fano_builder bvb (filesize,j+2);
		bvb.push_back(0);
		for (uint64_t i=0 ; i<filesize; i++){
			if (file[i] == '\n'){
				//strings[strings_index] = &file[i+1];
				bvb.push_back(i+1);
				//cout  << strings[strings_index]<<endl;
				//strings_index ++;
			}
		}
		
		bvb.push_back(filesize);

		succinct::elias_fano E(&bvb,false);

		cout << "Elias fano done" << endl;


	



		uint64_t tot_size = LCP_Builder(E,file,lcp_loc.c_str());
		cout << "Lcp vector done " << endl;

		struct stat sb2;
		int fd2;

		fd2 = open(lcp_loc.c_str(), O_RDONLY);
		fstat(fd2, &sb2);
		filesize = sb2.st_size;


		//map the file to memory address
		uint *lcps = (uint*)mmap(NULL, sb2.st_size, PROT_READ, MAP_SHARED, fd2, 0);
		

		TrieVisitSimulationSuccinct(E,file, &out, lcps,tot_size);


}
