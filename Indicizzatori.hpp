#include "Binary_search.h"
#include <utility>
#include <ctime>
#include <bitset>

using namespace std;

void Write_block_id(uint64_t* UC, uint64_t UC_size, int block , uint64_t strings, const string filename){
	//cout << "block: " << block << endl;
	cout << strings << " " << ((strings+block-1)/block)+1 << endl;
	succinct::elias_fano::elias_fano_builder bvb (strings,((strings+block-1)/block)+1);
	uint64_t* range = &UC[0];
	uint64_t cur_string = 0;
	uint64_t bloc_id=0;

	for (uint64_t i =0; i<((strings+block-1)/block); i++){
		if (cur_string < *range){
			//bvb.push_back(bloc_id);
		}
		else {
			range = range + 1;
			if (bloc_id < UC_size)
			bloc_id = bloc_id + 1;
		}
		cur_string = cur_string + block;
		//cout << "pushing " << bloc_id << endl;
		bvb.push_back(bloc_id);
}
	bvb.push_back(bloc_id);
	succinct::elias_fano A(&bvb,false);
	string outputfilename(filename.c_str());
	outputfilename = outputfilename+"_A_Vector";
	size_t dunque = succinct::mapper::freeze<succinct::elias_fano>(A, outputfilename.c_str());

}

void Write_block_id2(uint64_t* UC, uint64_t UC_size, int block , uint64_t strings, const string filename){
	//cout << "block: " << block << endl;
	cout << strings << " " << ((strings+block-1)/block)+1 << endl;
	std::vector<uint32_t> A;
	A.reserve(strings);
	//succinct::elias_fano::elias_fano_builder bvb (strings,((strings+block-1)/block)+1);
	uint64_t* range = &UC[0];
	uint64_t cur_string = 0;
	uint64_t bloc_id=0;

	for (uint64_t i =0; i<((strings+block-1)/block); i++){
		if (cur_string < *range){
			//bvb.push_back(bloc_id);
		}
		else {
			range = range + 1;
			if (bloc_id < UC_size)
			bloc_id = bloc_id + 1;
		}
		cur_string = cur_string + block;
		//cout << "pushing " << bloc_id << endl;
		A.push_back(bloc_id);
}
	A.push_back(bloc_id);
	string outputfilename(filename.c_str());
	outputfilename = outputfilename+"_A_Vector";
	std::ofstream OF (outputfilename.c_str(),ios::binary);
	OF.write((const char*)&A[0],sizeof(uint32_t)*A.size());

}

void Map_block_id(succinct::elias_fano &E,boost::iostreams::mapped_file_source &m,const string &A_file){

	m = boost::iostreams::mapped_file_source(A_file);
	size_t quindi = succinct::mapper::map<succinct::elias_fano>(E,m);
}

void Map_block_id2(std::vector<uint32_t> &E,const string &A_file){

	int fd = open(A_file.c_str(), O_RDONLY);
	struct stat sb;
		fstat(fd, &sb);
		uint64_t filesize = sb.st_size;
		//map the file to memory address
		uint32_t* addr = (uint32_t*)mmap(NULL, sb.st_size, PROT_READ, MAP_SHARED, fd, 0);
		E.reserve(filesize/sizeof(uint32_t));
		//E.push_back(0);
		for (uint64_t j=0; j<filesize/sizeof(uint32_t);j++){
			E.push_back(addr[j]);
		}


}

void breakTime( int seconds)
{
    clock_t temp;
    temp = clock () + seconds * CLOCKS_PER_SEC ;
    while (clock() < temp) {}
}


template <class ENC> class Father_I {
public:

	ENC enc;
	int Algorithm;
	uint Parameter;
	std::function<uint64_t(uint64_t)> fV;
	std::function<uint64_t(uint64_t)> fE;
	uint64_t start;
	char* addr;
	uint16_t suffix=0;
	uint16_t rear=0;
	int size;
	unsigned long diff=0;
	int BLOCK;
	uint Num_string;
	byte* to_extract;
	std::function<uint32_t(uint64_t)> fE_P;

	inline uint64_t Get_MyIndex(uint64_t u){
		//indice delle stringa copiata rispetto alle stringhe copiate
		uint64_t copiedclose = u/(Parameter);
		//indice della stringa copiata rispetto a tutte le stringhe
		return copiedclose * (Parameter);
	}

	inline uint64_t Get_MyIndex_C(uint64_t u){
		return u/Parameter;
	}

	inline uint64_t Get_MyIndex_F_C(uint64_t u){
		return u*Parameter;
	}

	virtual bool AllPosition(){
		return false;
	}

	virtual uint32_t Insert(const uint16_t to_encode,byte * &write){
		short tag = 0;
			tag = enc.Encode(to_encode,write);
			write = write + tag;
		return tag;
	}

	virtual void Set_addr(char* a){
		addr = a;
	}

	inline byte* GetNext(int*s){
		//cout <<"NEXT" <<endl;
		//cout <<"start: " << start << endl;
		to_extract = (byte*)&addr[start];                     
  	 	   
        to_extract = enc.Decode(to_extract,&rear);
  	 	rear = rear -1;  	
  	 //	cout << "rear " << rear << endl;
  	 	to_extract = enc.Decode(to_extract,&suffix);
  	 //	cout << "suffix: " << suffix << endl;
        *s = size - rear;
		diff = (unsigned long)to_extract - (unsigned long)&addr[start];
		start = start + diff + suffix;
		size = *s + suffix;
        return to_extract;   
	}

	inline byte* Get_F(uint64_t u){
		byte* a;
		return a;
	}

};


template<class T> class Succinct_All_Indexer : public Father_I<T>{
public:

succinct::elias_fano E;
boost::iostreams::mapped_file_source m;
succinct::rs_bit_vector V;
boost::iostreams::mapped_file_source mV;
succinct::elias_fano::select_enumerator SE;
uint64_t LIC;


Succinct_All_Indexer () : E() , V(), SE(E,0), LIC(0){

}

uint64_t Lengths_size(){
	return E.size();
}

uint64_t Indexes_size(){
	return V.size();
}

uint64_t num_Copied(){
	return V.num_ones();
}

virtual bool AllPosition(){
		return true;
	}

void Map(const string &Position_file, const string &Index_file,const string &A_file){

	
		m = boost::iostreams::mapped_file_source(Position_file);
	
		size_t quindi = succinct::mapper::map<succinct::elias_fano>(E,m);
		
		mV = boost::iostreams::mapped_file_source(Index_file);
		
		size_t dunque = succinct::mapper::map<succinct::rs_bit_vector>(V,mV);

		if (this->Algorithm == 0){
			this->fV = [&](uint64_t u){return V.select(V.rank(u+1)-1);};
		}
		else{
			this->fV = [&](uint64_t u){return this->Get_MyIndex(u);};
		}
		
		
}
//Get the index and the position of the copied string closest to u
//pair.first:index, pair.second:position
inline pair<byte*,uint64_t> Get(uint64_t u){
	pair<byte*,uint64_t> ret;
	//ret.first = V.select(V.rank(u+1)-1);
	ret.second = this->fV(u);
	uint64_t stringvpos = E.select(ret.second);
	ret.first = (byte*)&this->addr[stringvpos];
	SE = succinct::elias_fano::select_enumerator(E,ret.second+1);
    this->start = SE.next(); 
    this->size = this->start - stringvpos;
	return ret;
}

inline byte*  GetNext(int* s){
	int temp = this->start;
	this->to_extract = (byte*)&(this->addr[this->start]);   
	unsigned long diff = (unsigned long)this->to_extract;                  
    this->to_extract = this->enc.Decode(this->to_extract,&this->rear);
    diff = (unsigned long)this->to_extract - diff;
  	this->rear = this->rear -1;
  	this->start = SE.next();
  	this->size = this->size -this->rear;
  	*s = this->size;
  	this->size = this->size + this->start -(temp+diff);
	return  this->to_extract;
}

//deve ritornare l'indice della u-esima stringa copiata
inline uint64_t Get_Index_of_Copied(uint64_t u){
	LIC = V.select(u);
	SE = succinct::elias_fano::select_enumerator(E,LIC+1);
	return LIC;
}

inline uint64_t Get_position(uint64_t u){
	return E.select(LIC);
}


void Freeze (uint64_t* pos_vector, uint64_t filesize, uint64_t* U_vector, uint64_t uncompressed_size, string outputfilename, uint64_t strings){
		
		std::vector<bool> VB(strings);
		string o = outputfilename+"_V_vector";
		uint64_t LastUncompressed = 0;
		uint64_t vIx = 0;
		uint64_t k = 0;
		while (k<uncompressed_size){
			if (vIx == U_vector[k]){
				VB[vIx]=true;
				++k;
			}
			else {
				VB[vIx]=false;
			}
			vIx++;
		}
		LastUncompressed = vIx -1;
		//cout <<LastUncompressed<< endl;
		for (uint64_t i =vIx; i<strings; i++){
			VB[vIx]=false;
		}
		succinct::bit_vector_builder BVBV;
		for (uint64_t i=0; i<VB.size(); i++){
			BVBV.push_back(VB[i]);
		}   
		//VB.clear();
		succinct::rs_bit_vector V2(&BVBV);
		
		size_t quindi = succinct::mapper::freeze<succinct::rs_bit_vector>(V2, o.c_str());
		    
		succinct::elias_fano::elias_fano_builder bvb (filesize,strings+1);
		for(uint64_t i=0; i < strings;i++){
			bvb.push_back(pos_vector[i]);	
		}
		
		bvb.push_back(filesize);
		succinct::elias_fano E2(&bvb,false);
		
		o = outputfilename+"_Elias";
		size_t dunque = succinct::mapper::freeze<succinct::elias_fano>(E2, o.c_str());

	}


};

//questa mette gli indici in array binario e posizioni stringhe copiate in Elias-Fano
template<typename T>
class Succinct_Pos_Indexer : public Father_I<T> {
	public:

	succinct::elias_fano E;
	boost::iostreams::mapped_file_source m1;
	succinct::rs_bit_vector V;
	boost::iostreams::mapped_file_source mV1;
	uint64_t temp=0;


Succinct_Pos_Indexer () : E(), V() {}

uint64_t Lengths_size(){
	return E.size();
}

uint64_t Indexes_size(){
	return V.size();
}

uint64_t num_Copied(){
	return V.num_ones();
}

void Map(const string &Position_file, const string &Index_file, const string &A_file){

		m1 =  boost::iostreams::mapped_file_source(Position_file);
	
		size_t quindi = succinct::mapper::map<succinct::elias_fano>(E,m1);
		
		mV1 =  boost::iostreams::mapped_file_source(Index_file);
		
		size_t dunque = succinct::mapper::map<succinct::rs_bit_vector>(V,mV1);

		if (this->Algorithm == 0){
			this->fV = [&](uint64_t u){return V.rank(u+1);};
			this->fE = [&](uint64_t u){return V.select(u-1);};
		}
		else{
			this->fV = [&](uint64_t u){return this->Get_MyIndex_C(u)+1;};
			this->fE = [&](uint64_t u){return this->Get_MyIndex_F_C(u);};
		}

		
}

void Freeze (uint64_t* pos_vector, uint64_t filesize, uint64_t* U_vector, uint64_t uncompressed_size, string outputfilename, uint64_t strings){
		std::vector<bool> VB(strings);
		uint64_t LastUncompressed = 0;
		uint64_t vIx = 0;
		uint64_t k = 0;
		while (k<uncompressed_size){
			if (vIx == U_vector[k]){
				VB[vIx]=true;
				++k;
			}
			else {
				VB[vIx]=false;
			}
			vIx++;
		}
		LastUncompressed = vIx -1;
		//cout <<LastUncompressed<< endl;
		for (uint64_t i =vIx; i<strings; i++){
			VB[vIx]=false;
		}
        
		succinct::bit_vector_builder BVBV;
		for (int i=0; i<VB.size(); i++){
			BVBV.push_back(VB[i]);
		}
        
		VB.clear();
		succinct::rs_bit_vector V2(&BVBV);
		
		string o = outputfilename+"_V_vector";
		size_t quindi = succinct::mapper::freeze<succinct::rs_bit_vector>(V2, o.c_str());
      
		succinct::elias_fano::elias_fano_builder bvb (filesize,(V2.num_ones()));
		
		for(uint64_t i=0; i<V2.num_ones();i++){
			bvb.push_back(pos_vector[i]);	
		}

		succinct::elias_fano E2(&bvb,false);	

		o = outputfilename+"_Elias";
		size_t dunque = succinct::mapper::freeze<succinct::elias_fano>(E2, o.c_str());
	
	}

	inline pair<byte*,uint64_t>Get(uint64_t u){
		pair<byte*,uint64_t> ret;
		//uint64_t v = V.rank(u+1);
		temp = this->fV(u);
		//cout << "temp: " << temp<<endl;
        //position of the copied string closest to u
       	uint64_t stringvpos = E.select(temp-1);
       	ret.second=temp;
       	this->to_extract = (byte*)(&this->addr[stringvpos]);
       	this->diff = (unsigned long)this->to_extract;
   	 	this->to_extract = this->enc.Decode(this->to_extract,&this->suffix);
    	this->diff = (unsigned long)this->to_extract - this->diff;
        ret.first = this->to_extract;
        this->start = stringvpos+this->suffix+this->diff;    
        this->size = this->suffix;
       	return pair<byte*,uint64_t>(this->to_extract,this->fE(temp));
	}

	

	//deve ritornare l'indice della u-esima stringa copiata
inline uint64_t Get_Index_of_Copied(uint64_t u){
	return V.select(u);
}

inline uint64_t Get_position(uint64_t u){
	return E.select(u);
}

};

//questa mette gli indici in un array di interi e le posizioni delle strnghe copiate in Elias-Fano
template<typename T>
class Semi_Succinct : public Father_I<T>{
public:
	succinct::elias_fano E;
	boost::iostreams::mapped_file_source m;
	std::vector<uint32_t> Copied_indexes;
	std::vector<uint32_t>::iterator it;
	uint64_t key=0;

	Semi_Succinct() : E(){}

uint64_t Lengths_size(){
	return E.size();
}

uint64_t Indexes_size(){
	return Copied_indexes.size();
}

uint64_t num_Copied(){
	return Copied_indexes.size();
}


void Freeze (uint64_t* pos_vector, uint64_t filesize, uint64_t* U_vector, uint64_t uncompressed_size, string outputfilename, uint64_t strings){
	uint32_t *index_vector = (uint32_t*)malloc(uncompressed_size*sizeof(uint32_t));
	for(uint64_t i=0; i<uncompressed_size; i++){
		index_vector[i] = (uint32_t)U_vector[i];
	}
	string o = outputfilename+"_V_vector";
	std::ofstream Pos_vector(o.c_str(),ios::binary);
	Pos_vector.write((const char*)&index_vector[0], uncompressed_size*sizeof(uint32_t));

	succinct::elias_fano::elias_fano_builder bvb (filesize,uncompressed_size);
		
		for(uint64_t i=0; i<uncompressed_size;i++){
			bvb.push_back(pos_vector[i]);	
		}

		succinct::elias_fano E2(&bvb,false);	
		o = outputfilename+"_Elias";
		size_t dunque = succinct::mapper::freeze<succinct::elias_fano>(E2, o.c_str());
		free(index_vector);

}
void Map(const string &Position_file, const string &Index_file,const string &A_file){

		m = boost::iostreams::mapped_file_source(Position_file);
	
		size_t quindi = succinct::mapper::map<succinct::elias_fano>(E,m);
		
		struct stat sb;
		int fd;

		fd = open(Index_file.c_str(), O_RDONLY);
		fstat(fd, &sb);
		uint64_t filesize = sb.st_size;
		//map the file to memory address
		uint32_t* addr = (uint32_t*)mmap(NULL, sb.st_size, PROT_READ, MAP_SHARED, fd, 0);
		Copied_indexes.reserve(filesize/sizeof(uint32_t));
		for (uint64_t j=0; j<filesize/sizeof(uint32_t);j++){
			Copied_indexes.push_back(addr[j]);
		}
		if (this->Algorithm == 0){
			this->fV = [&](uint64_t u){it = std::upper_bound(Copied_indexes.begin(),Copied_indexes.end(),u); 
				return it -1 -Copied_indexes.begin();};
			this->fE = [&](uint64_t u){return Copied_indexes[u];};
		}
		else{
			this->fV = [&](uint64_t u){return this->Get_MyIndex_C(u);};
			this->fE = [&](uint64_t u){return this->Get_MyIndex_F_C(u);};
		}
}

inline pair<byte*,uint64_t>Get(uint64_t u){
	//uint key = binary_search<uint32_t>(Copied_indexes,0,Copied_indexes.size()-1,u);
	//it = std::upper_bound(Copied_indexes.begin(),Copied_indexes.end(),u);
	key = this->fV(u);
	uint64_t stringvpos = E.select(key);
	this->to_extract = (byte*)this->addr+stringvpos;
	this->diff = (unsigned long)this->to_extract;
    this->to_extract = this->enc.Decode(this->to_extract,&this->suffix);
    this->diff = (unsigned long)this->to_extract - this->diff;
   // cout << "very diff: " << diff << endl;
    this->size = this->suffix;
    this->start = stringvpos + this->suffix + this->diff; 
	return pair<byte*,uint64_t>(this->to_extract,this->fE(key));
}


//deve ritornare l'indice della u-esima stringa copiata
inline uint64_t Get_Index_of_Copied(uint64_t u){
	return Copied_indexes[u];
}

inline uint64_t Get_position(uint64_t u){
	return E.select(u);
}

};

template<typename T>
class Semi_Succinct_All : public Semi_Succinct<T>{
public:

	succinct::elias_fano::select_enumerator SE;
	uint64_t LIC;

	Semi_Succinct_All() : SE(this->E,0) , LIC(0){}

	virtual bool AllPosition(){
		return true;
	}

	void Freeze (uint64_t* pos_vector, uint64_t filesize, uint64_t* U_vector, uint64_t uncompressed_size, string outputfilename, uint64_t strings){
		    
		succinct::elias_fano::elias_fano_builder bvb (filesize,strings+1);
		for(uint64_t i=0; i < strings;i++){
			
			bvb.push_back(pos_vector[i]);	
		}
		bvb.push_back(filesize);
		succinct::elias_fano E2(&bvb,false);
		string o = outputfilename+"_Elias";
		size_t dunque = succinct::mapper::freeze<succinct::elias_fano>(E2, o.c_str());

		uint32_t *index_vector = (uint32_t*)malloc(uncompressed_size*sizeof(uint32_t));
		for(uint64_t i=0; i<uncompressed_size; i++){
			index_vector[i] = (uint32_t)U_vector[i];
		}

		o = outputfilename+"_V_vector";
		std::ofstream Pos_vector(o.c_str(),ios::binary);
		Pos_vector.write((const char*)&index_vector[0], uncompressed_size*sizeof(uint32_t));
		free(index_vector);
	}

inline pair<byte*,uint64_t> Get(uint64_t u){
	pair<byte*,uint64_t> ret;
	//ret.first = V.select(V.rank(u+1)-1);
	ret.second = this->fE(this->fV(u));
	uint64_t stringvpos = this->E.select(ret.second);
	ret.first = (byte*)&this->addr[stringvpos];
	SE = succinct::elias_fano::select_enumerator(this->E,ret.second+1);
    this->start = SE.next(); 
    this->size = this->start - stringvpos;
	return ret;
}

inline byte*  GetNext(int* s){
	int temp = this->start;
	this->to_extract = (byte*)&(this->addr[this->start]);   
	unsigned long diff = (unsigned long)this->to_extract;                  
    this->to_extract = this->enc.Decode(this->to_extract,&this->rear);
    diff = (unsigned long)this->to_extract - diff;
  	this->rear = this->rear -1;
  	this->start = SE.next();
  	this->size = this->size -this->rear;
  	*s = this->size;
  	this->size = this->size + this->start -(temp+diff);
	return  this->to_extract;
}



inline uint64_t Get_Index_of_Copied(uint64_t u){
	LIC = this->Copied_indexes[u];
	SE = succinct::elias_fano::select_enumerator(this->E,LIC+1);
	return LIC;
}

inline uint64_t Get_position(uint64_t u){
	return this->E.select(LIC);
}



};

//questa mette entrambi in array di interi
template<typename T,typename F>
class Plain_Indexer : public Father_I<F>{
public:

	std::vector<uint32_t> Copied_indexes;
	std::vector<uint32_t>::iterator it;
	std::vector<T> Copied_positions;
	uint64_t CS;
	std::vector<uint32_t>::iterator CB;
	std::vector<uint32_t>::iterator CE;
	uint64_t key=0;
	uint64_t stringvpos = 0;

uint64_t Lengths_size(){
	return Copied_positions.size();
}

uint64_t Indexes_size(){
	return Copied_indexes.size();
}
uint64_t num_Copied(){
	return Copied_positions.size();
}

void Freeze (uint64_t* pos_vector, uint64_t filesize, uint64_t* U_vector, uint64_t uncompressed_size, string outputfilename, uint64_t strings){
	uint32_t *index_vector = (uint32_t*)malloc(uncompressed_size*sizeof(uint32_t));
	for(uint64_t i=0; i<uncompressed_size; i++){
		index_vector[i] = (uint32_t)U_vector[i];
	}
	string o = outputfilename+"_V_vector";
	std::ofstream Pos_vector(o.c_str(),ios::binary);
	Pos_vector.write((const char*)&index_vector[0], uncompressed_size*sizeof(uint32_t));
	free(index_vector);

	o = outputfilename+"_Elias";
	std::ofstream Pos_file(o.c_str(),ios::binary);

	
		T* positions = (T*)malloc(uncompressed_size*sizeof(T));
		for (uint64_t j=0; j<uncompressed_size; j++){
			positions[j] = (T)pos_vector[j];
		}
		Pos_file.write((const char*)&positions[0], uncompressed_size*sizeof(T));
		free(positions);
		//cout << "OK till now" << endl;
		//Write_block_id(U_vector,uncompressed_size,this->BLOCK,strings,outputfilename.c_str());
	}


void Map(const string &Position_file, const string &Index_file, const string &A_file){
		struct stat sb,sb_P;
		int fd,fd_P;

		fd = open(Index_file.c_str(), O_RDONLY);
		fstat(fd, &sb);
		uint64_t filesize = sb.st_size;
		//map the file to memory address
		uint32_t* addr = (uint32_t*)mmap(NULL, sb.st_size, PROT_READ, MAP_SHARED, fd, 0);
		//Copied_indexes.push_back(0);
		Copied_indexes.reserve(filesize/sizeof(uint32_t));
		for (uint64_t j=0; j<filesize/sizeof(uint32_t);j++){
			Copied_indexes.push_back(addr[j]);
		}

		//int g = munmap(addr,sb.st_size);
		//sb.close();
		//cout << g << endl;
		
		
		fd_P = open(Position_file.c_str(), O_RDONLY);
		fstat(fd_P, &sb_P);
		uint64_t filesize_P = sb_P.st_size;
		//map the file to memory address
		T* addr_P = (T*)mmap(NULL, sb_P.st_size, PROT_READ, MAP_SHARED, fd_P, 0);
		Copied_positions.reserve(filesize_P/sizeof(T));
		//Copied_positions.push_back(0);
		for (uint64_t j=0; j<filesize_P/sizeof(T);j++){
			Copied_positions.push_back(addr_P[j]);
		}

		//g = munmap(addr_P,sb_P.st_size);
		close(fd_P);
		close(fd);
		//cout << g << endl;

		CS = Copied_positions.size();
		CB = Copied_indexes.begin();
		CE= Copied_indexes.end();

		for (int i=0; i<5;i++){
			cout << Copied_indexes[i] << " ";
		}
		cout << endl;
		for (int i=0; i<5;i++){
			cout << Copied_positions[i] << " ";
		}

		this->Num_string = Copied_indexes[Copied_positions.size()-1];

		if (this->Algorithm == 0){
			this->fV = [&](uint64_t u){it = std::upper_bound(CB,CE,u); 
				return it -1 -CB;};
			this->fE = [&](uint64_t u){return Copied_indexes[u];};
			this->fE_P = this->fE;
		}
		else{
			this->fV = [&](uint64_t u){return this->Get_MyIndex_C(u);};
			this->fE = [&](uint64_t u){return this->Get_MyIndex_F_C(u);};
			this->fE_P = [&] (uint64_t u) {
				
				if (u == CS-1){
					return (uint64_t)this->Num_string;
				}
				else {
					return (uint64_t)this->Get_MyIndex_F_C(u);
				}

			};
		}



}



inline pair<byte*,uint64_t>Get(uint64_t u){
	//uint key = binary_search<uint32_t>(Copied_indexes,0,Copied_indexes.size()-1,u);
	//it = std::upper_bound(Copied_indexes.begin(),Copied_indexes.end(),u);
	key = this->fV(u);
	//cout << "key: " << key << endl;
	stringvpos = Copied_positions[key];
	//cout <<"stringvpos: " << stringvpos << endl;
	this->to_extract = (byte*)this->addr+stringvpos;
	//cout << "to_extract in function: " << (unsigned long)this->to_extract << endl;
	this->diff = (unsigned long)this->to_extract;
	//cout << "diff in function: "<< (unsigned long)this->diff << endl;
	//bitset<8> x(this->to_extract[0]);
	//cout << "bitset: " << x << endl; 
    this->to_extract = this->enc.Decode(this->to_extract,&this->suffix);
    //cout << "IO... " << this->suffix << endl;
    this->diff = (unsigned long)this->to_extract - this->diff;
    //cout << "very diff: " << this->diff << endl;
    this->size = this->suffix;
    this->start = stringvpos + this->suffix + this->diff; 
	return pair<byte*,uint64_t>(this->to_extract,this->fE(key));
}

inline byte* Get_F(uint64_t u){
	//key = this->fV(u);
	//cout << "key: " << key << endl;
	//cout << "GET" << endl;
	stringvpos = Copied_positions[u];
	//cout <<"stringvpos: " << stringvpos << endl;
	this->to_extract = (byte*)this->addr+stringvpos;
	this->diff = (unsigned long)this->to_extract;
    this->to_extract = this->enc.Decode(this->to_extract,&this->suffix);
    //cout << "suffix: " << this->suffix << endl;
    this->diff = (unsigned long)this->to_extract - this->diff;
    //cout << "very diff: " << this->diff << endl;
    this->size = this->suffix;
    this->start = stringvpos + this->suffix + this->diff; 
    return this->to_extract;
}

 inline uint64_t Get_Index_of_Copied(uint64_t u){
 	//cout << this->fE_P(u) << " "<<Copied_indexes[u]<< " "<< u<<endl;
	return this->fE_P(u);
	//return Copied_indexes[u];

}

inline uint64_t Get_position(uint64_t u){
	return Copied_positions[u];
}
};

template <typename T,typename F>
class Plain_with_A : public Plain_Indexer<T,F> {
public:
	succinct::elias_fano A;
	boost::iostreams::mapped_file_source m;
	std::vector<uint32_t> AA;

	void Freeze (uint64_t* pos_vector, uint64_t filesize, uint64_t* U_vector, uint64_t uncompressed_size, string outputfilename, uint64_t strings){
		Plain_Indexer<T,F>::Freeze(pos_vector,filesize,U_vector,uncompressed_size,outputfilename,strings);
		//cout << "super freezed" << endl;
		//Write_block_id(U_vector,uncompressed_size,this->BLOCK,strings,outputfilename.c_str());
		Write_block_id2(U_vector,uncompressed_size,this->BLOCK,strings,outputfilename.c_str());

	}

	void Map(const string &Position_file, const string &Index_file, const string &A_file){
		Plain_Indexer<T,F>::Map(Position_file,Index_file,A_file);
		//Map_block_id(A,m,A_file);
		Map_block_id2(AA,A_file);

		//cout << this->Copied_indexes.size() << " " << A.num_ones() << endl;

		if (this->Algorithm == 0 && this->BLOCK>1){
			this->fV = [&](uint64_t u){
		//uint64_t b1= A.select((u/this->BLOCK)+1);
		//cout <<"b1: " << b1 << " u: "<<u<<endl;
				uint32_t b1 = AA[(u/this->BLOCK)+1];
				if (this->Copied_indexes[b1-1] > u){
				//	b1 = A.select((u/this->BLOCK));
				//	cout <<"b1: " << b1 << endl;
					b1 = AA[(u/this->BLOCK)];
				}
			//	cout << "b1: " << b1 << endl;
				return b1-1;
			};
		}
		if (this->Algorithm == 0 && this->BLOCK == 1){
			cout << "Mapping with block=1" << endl;
			this->fV = [&](uint64_t u){
				//return A.select(u)-1;
				return AA[u]-1;
			};
		}
	}



};

template<typename T,typename F>
class E_plain_V_succinct : public Father_I<F>{
public:

std::vector<T> Copied_positions;
succinct::rs_bit_vector V;
boost::iostreams::mapped_file_source m;
uint64_t key;

E_plain_V_succinct() : V() {}

void Freeze (uint64_t* pos_vector, uint64_t filesize, uint64_t* U_vector, uint64_t uncompressed_size, string outputfilename, uint64_t strings){
	std::vector<bool> VB(strings);
		uint64_t LastUncompressed = 0;
		uint64_t vIx = 0;
		uint64_t k = 0;
		while (k<uncompressed_size){
			if (vIx == U_vector[k]){
				VB[vIx]=true;
				++k;
			}
			else {
				VB[vIx]=false;
			}
			vIx++;
		}
		LastUncompressed = vIx -1;
		//cout <<LastUncompressed<< endl;
		for (uint64_t i =vIx; i<strings; i++){
			VB[vIx]=false;
		}
        
		succinct::bit_vector_builder BVBV;
		for (int i=0; i<VB.size(); i++){
			BVBV.push_back(VB[i]);
		}
        
		VB.clear();
		succinct::rs_bit_vector V2(&BVBV);
		
		string o = outputfilename+"_V_vector";
		size_t quindi = succinct::mapper::freeze<succinct::rs_bit_vector>(V2, o.c_str());

		o = outputfilename+"_Elias";
		std::ofstream Pos_file(o.c_str(),ios::binary);
		

		T* positions = (T*)malloc(uncompressed_size*sizeof(T));
		for (uint64_t j=0; j<uncompressed_size; j++){
			positions[j] = (T)pos_vector[j];
		}
		Pos_file.write((const char*)&positions[0], uncompressed_size*sizeof(T));
		free(positions);

}

void Map(const string &Position_file, const string &Index_file,const string &A_file){

		struct stat sb_P;
		int fd_P;

		fd_P = open(Position_file.c_str(), O_RDONLY);
		fstat(fd_P, &sb_P);
		uint64_t filesize_P = sb_P.st_size;
		//map the file to memory address
		T* addr_P = (T*)mmap(NULL, sb_P.st_size, PROT_READ, MAP_SHARED, fd_P, 0);
		Copied_positions.reserve(filesize_P/sizeof(T));
		for (uint64_t j=0; j<filesize_P/sizeof(T);j++){
			Copied_positions.push_back(addr_P[j]);
		}

		m =  boost::iostreams::mapped_file_source(Index_file);
	
		
		size_t dunque = succinct::mapper::map<succinct::rs_bit_vector>(V,m);

		if (this->Algorithm == 0){
			this->fV = [&](uint64_t u){return V.rank(u+1);};
			this->fE = [&](uint64_t u){return V.select(u-1);};
		}
		else{
			this->fV = [&](uint64_t u){return this->Get_MyIndex_C(u);};
			this->fE = [&](uint64_t u){return this->Get_MyIndex_F_C(u);};
		}

	

}

inline pair<byte*,uint64_t>Get(uint64_t u){
	//uint key = binary_search<uint32_t>(Copied_indexes,0,Copied_indexes.size()-1,u);
	//it = std::upper_bound(Copied_indexes.begin(),Copied_indexes.end(),u);
	key = this->fV(u);
	uint64_t stringvpos = Copied_positions[key-1];
	this->to_extract = (byte*)this->addr+stringvpos;
	this->diff = (unsigned long)this->to_extract;
    this->to_extract = this->enc.Decode(this->to_extract,&this->suffix);
    this->diff = (unsigned long)this->to_extract - this->diff;
   // cout << "very diff: " << diff << endl;
    this->size = this->suffix;
    this->start = stringvpos + this->suffix + this->diff; 
	return pair<byte*,uint64_t>(this->to_extract,this->fE(key));


}


inline uint64_t Get_Index_of_Copied(uint64_t u){
	return V.select(u);
}

inline uint64_t Get_position(uint64_t u){
	return Copied_positions[u];
}
uint64_t num_Copied(){
	return V.num_ones();
}


};





